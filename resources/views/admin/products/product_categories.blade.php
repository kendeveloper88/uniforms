@extends('admin.products.product_layout')
@section('content')
    <?php
    $product_categories =  App\Product::get_product_categories();
    ?>
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-12">
                    <ol class="breadcrumb float-sm-left">
                        <li class="breadcrumb-item"><a href="{{ route('admin') }}">Dashboard</a></li>
                        <li class="breadcrumb-item active">Products Categories</li>
                    </ol>
                </div>
            </div>
        </div><!-- /.container-fluid -->
    </section>
    <!-- Main content -->
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-4">
                    <!-- general form elements -->
                    <div class="card card-primary">
                        <div class="card-header">
                            <h3 class="card-title">Add new product category</h3>
                        </div>
                        <!-- /.card-header -->
                        <!-- form start -->
                        <form role="form" id="form-create-category">
                            <div class="card-body">
                                <div class="form-group">
                                    <label>Category title</label>
                                    <input type="text" class="form-control" name="name" placeholder="Enter name" data-title="Name" data-required="true">
                                    <span class="um-field-error d-none"></span>
                                </div>
                                <div class="form-group">
                                    <label>Description</label>
                                    <textarea class="form-control" name="description" placeholder="Enter description" data-title="Description" data-required="false"></textarea>
                                </div>
                                <div class="form-group">
                                    <label>Select parent</label>
                                    <select class="form-control" name="parent_id" data-title="Parent" data-required="false">
                                        <option value="">No parent</option>
                                        @if($product_categories)
                                            @foreach($product_categories as $category)
                                        <option value="{{ $category->id }}">{{ $category->name }}</option>
                                            @endforeach
                                        @endif
                                    </select>
                                </div>
                            </div>
                            <!-- /.card-body -->
                            <div class="card-footer">
                                <button type="button" onclick="create_category()" id="button-save-category" class="btn btn-primary">Save <span class="d-none"><i class="fas fa-spinner fa-spin"></i></span></button>
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="card">
                        <div class="card-header">
                            <h3 class="card-title">Products Categories ({{ $product_categories->total() }})</h3>

                            <div class="card-tools">
                                <div class="input-group input-group-sm" style="width: 150px;">
                                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                                    <div class="input-group-append">
                                        <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- /.card-header -->
                        <div class="card-body table-responsive p-0">
                            <table class="table table-head-fixed text-nowrap table-hover">
                                <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Category</th>
                                    <th>Slug</th>
                                    <th>Last Modified</th>
                                    <th>Actions</th>
                                </tr>
                                </thead>
                                <tbody id="display_categories">
                                @if (count($product_categories) > 0)
                                    @foreach ($product_categories as $category)
                                        <tr data-category-id="{{ $category->id }}">
                                            <td>{{ $category->id }}</td>
                                            <td>{{ $category->name }}</td>
                                            <td>{{ $category->slug }}</td>
                                            <td>{{ $category->updated_at }}</td>
                                            <td>
                                                <button class="btn btn-info btn-sm" onclick="edit_category({{ json_encode($category) }})" data-toggle="modal" data-target="#CategoryModal">
                                                    <i class="fas fa-pencil-alt">
                                                    </i>
                                                    Edit
                                                </button>
                                                <button class="btn btn-danger btn-sm" onclick="delete_category({{ $category->id }})">
                                                    <i class="fas fa-trash">
                                                    </i>
                                                    Delete
                                                </button>
                                            </td>
                                        </tr>
                                    @endforeach
                                @else
                                    <tr>
                                        <td colspan="8" class="text-center">No products exist.</td>
                                    </tr>
                                @endif

                                </tbody>
                            </table>
                        </div>
                        <!-- /.card-body -->
                    </div>
                    <!-- /.card -->
                    <nav aria-label="Page navigation example">
                        <ul class="pagination">
                            <li class="page-item"><a class="page-link" href="{{ $product_categories->previousPageUrl() }}">Previous</a></li>
                            <li class="page-item"><span class="page-link">{{ $product_categories->currentPage() }}</span></li>
                            <li class="page-item"><a class="page-link" href="{{ $product_categories->nextPageUrl() }}">Next</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>
    </section>
@endsection
