<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
    <title>Unipro</title>
    <link rel="icon" href="{{ asset('images/favicon.png') }}" />
    <link href="https://fonts.googleapis.com/css2?family=Rajdhani:wght@300;400;500;600;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('plugins/fontawesome-5.14.0/css/all.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/bootstrap-4.5.3/css/bootstrap.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/sweetalert2/dist/sweetalert2.min.css') }}" />
    <link rel="stylesheet" href="{{ asset('plugins/owlcarousel2-2.3.4/assets/owl.carousel.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/owlcarousel2-2.3.4/assets/owl.theme.default.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/style.css') }}" />
    <link rel="stylesheet" href="{{ asset('css/custom.css') }}" />
</head>
<body>
<header>
    <!--Header Top-->
    <div class="header-top">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-6">
                    <div class="headline">Uniforms for the professionals.</div>
                </div>
                <div class="col-md-6 col-md-6 text-right">
                    <ul class="head-contact">
                      <li><a href="#"><i class="fas fa-phone-alt"></i> Toll free: (888) 691-6200</a></li>
                      <li class="border-left"><a href="#">Login</a></li>
                      <li class="border-left"><a href="#">Register</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!--Header main-->
    <div class="header-main">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo">
                        <a href="{{ url('/') }}"><img src="{{ url('images/logo_uniforms.png') }}"></a>
                    </div>
                </div>
                <div class="col-md-6"> {!! display_menu($slug) !!}</div>
                <div class="col-md-3 head-mini-cart text-right">
                    <div class="cart">Your card: <a href="#"><i class="fas fa-shopping-cart"></i><span id="mini-cart" class="badge badge-dark badge-shopping-cart">0</span></a></div>
                    <div class="search-product">
                        <input type="text" placeholder="Search" name="search" autocomplete="off">
                        <button type="button"><i class="fas fa-search"></i></button>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
@yield('content')
<footer class="footer-area pt-5 pl-3 pr-3 pb-3">
    <div class="container-fluid">
        <div class="row justify-content-between">
            <div class="col-md-4">
                <div class="single-footer-widget footer_1">
                    <div class="logo_footer">
                        <a href="{{ url('/') }}"><img src="{{ asset('images/logo_uniforms.png') }}" alt="logo" /> </a>
                    </div>
                    <label class="mt-5">Follows us on</label>
                    <ul class="social">
                        <li><a class="social-facebook"><i class="fab fa-facebook-f"></i></a></li>
                        <li><a class="social-twitter"><i class="fab fa-twitter"></i></a></li>
                        <li><a class="social-instagram"><i class="fab fa-instagram"></i></a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer_2">
                    <h4>Customer service</h4>
                    <ul class="service">
                        <li><a>My account</a></li>
                        <li><a>Order history</a></li>
                        <li><a>Find order</a></li>
                        <li><a>Return & Exchange</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer_2">
                    <h4>About us</h4>
                    <ul class="pages">
                        <li><a>About us</a></li>
                        <li><a>Order history</a></li>
                        <li><a>Find order</a></li>
                        <li><a>Return & Exchange</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer_2">
                    <h4>Contact us</h4>
                    <ul class="contacts">
                        <li><a><i class="fas fa-headphones-alt"></i>Online 8:00 AM - 6:00 PM</a></li>
                        <li class="mt-2"><a><i class="fas fa-map-marker-alt"></i>390 Nye Avenue, Irvington NJ 07111</a></li>
                        <li class="mt-2"><a><i class="far fa-envelope"></i>info@uniprouniforms.com</a></li>
                        <li class="mt-2"><a><i class="fas fa-phone-volume"></i>Toll free: (888) 591-6200</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-2">
                <div class="footer_2">
                    <div class="partner text-center">
                        <p class="mt-2"><a href="#"><img src="{{ url('images/authorize.png')  }}"></a></p>
                        <p class="mt-2"><a href="#"><img src="{{ url('images/wbenc.png')  }}"></a></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="copy-right text-center">
            <p>2020 Unipro international. All rights reserved.</p>
        </div>
    </div>
</footer>
<script src="{{ asset('plugins/jQuery/v2.2.4/jquery-2.2.4.min.js') }}"></script>
<script src="{{ asset('plugins/bootstrap-4.5.3/js/bootstrap.min.js') }}"></script>
<script src="{{ asset('plugins/owlcarousel2-2.3.4/owl.carousel.min.js') }}"></script>
<script src="{{ asset('plugins/jQuery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/jQuery-ui-1.11.4.custom/jquery-ui.min.js') }}"></script>
<script src="{{ asset('plugins/sweetalert2/dist/sweetalert2.all.min.js') }}"></script>
<script src="{{ asset('js/custom.js') }}"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script>
<?php
        $cart = Request::session()->get('cart');
?>
    var cart = {products:[]};
    @if(isset($cart) && isset($cart['products']))
        cart = {!! json_encode($cart) !!}
    @endif
    var setting = {
        'upload_ajax_url':'{{ url('nopriv_upload') }}',
        'ajax_url':'{{ url('nopriv_ajax') }}',
        'token':'{{ csrf_token() }}',
    };
     @if( Auth::check() && isset($slug) && $slug=='account')

    var profile_meta = [];
    $('.job-details-wrapper [name]').each(function(){
        var k = $(this).attr('name');
        var t = $(this).data('type');
        if(t=='meta')profile_meta.push({name:k,value:''});
    })
    $.ajax({
        url:setting.ajax_url,
        type:'post',
        data:{data: profile_meta ,action:'get_all_user_meta',_token:setting.token},
        success: function(resulf){
          if(resulf){
              resulf = JSON.parse(resulf);
              resulf.forEach( function(meta,index){
                  $('.job-details-wrapper [name="'+meta.name+'"]').val( meta.value );
              } )
          }
        }
    })

    // upload file media
    $('[name="UploadMedia"]').on('change',function(){
        var id = $(this).attr('data-media');
        var media = $(this)[0].files[0];
        var formData = new FormData();
        formData.append('UploadMedia', media);
        formData.append('_token', setting.token);
        $.ajax({
            url : setting.upload_ajax_url,
            type : 'POST',
            data : formData,
            processData: false,  // tell jQuery not to process the data
            contentType: false,  // tell jQuery not to set contentType
            success : function(resulf) {
                if(resulf){

                    resulf = JSON.parse(resulf);
                    console.log(resulf);
                    if(resulf['success']){
                        $('[name="UploadMedia"]').val('');
                        $('#frmEditProfileImage img').attr('src',resulf.link);
                        $('#frmEditProfileImage [name="avata"]').val(resulf.id);
                        save_account('#frmEditProfileImage');
                        $('#MediaModal').modal('hide');

                    }

                }

            }
        })
    });



    @endif
</script>
</body>
</html>
