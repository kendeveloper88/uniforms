@extends('layouts.layout_main')

@section('content')
    <header class="main_menu single_page_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logos/kindward_logo.png') }}" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                           {!! display_menu() !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="breadcrumb breadcrumb_bg" style="background-image:url('{{ asset('img/backgrounds/background_005.jpg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-lock teal"></i><br>Account Login</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial_part push-top-25">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p>Account Services</p>
                        <h2 class="teal">Account Login</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-6">
                    <div class="login-page-content">
                        <div class="login-form">
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="col-md-12 col-centered">
                                        <div class="basic-login">
                                            <form id="frmUserLogin" name="frmUserLogin" action="{{ url('/loginsite') }}" method="post" role="form">
                                                {{ csrf_field() }}
                                                <input id="action" name="action" type="hidden" value="do_login">
                                                <div class="form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                                    <label for="email"><i class="fa fa-envelope"></i> Username</label>
                                                    <input id="email" type="text" class="form-control" name="username" value="{{ old('username') }}" placeholder="Username" required autofocus>

                                                    @if ($errors->has('username'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('username') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                                    <label for="password"><i class="fa fa-lock"></i> Password</label>
                                                    <input id="password" type="password" class="form-control" name="password" placeholder="Password" required>

                                                    @if ($errors->has('password'))
                                                        <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                                    @endif
                                                </div>
                                                <div class="form-group" style="margin-left:20px;">
                                                    <label class="checkbox">
                                                        <input type="checkbox" name="remember" {{ old('remember') ? 'checked' : ''}}> Remember Me
                                                    </label>
                                                </div>
                                                <div class="form-group text-center">
                                                    <a href="{{ url('/password/reset') }}" class="forgot-password dark-purple" style="margin-bottom:10px !important;">Forgot password?</a><br>
                                                    <button type="button" id="btnRegister" name="btnRegister" class="btn btn-purple pull-left min-w175 " onclick="location.href='{{ route('register') }}';"><i class="fa fa-user-plus"></i> Not Registered?</button>
                                                    <button type="submit" id="btnSubmit" name="btnSubmit" class="btn btn-primary pull-right min-w175 ml-2 "><i class="fa fa-lock"></i> Login</button>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- /.login-box -->
@endsection