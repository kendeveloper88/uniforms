<?php
$args = [
    array('name'=>'post_type','operator'=>'=','value'=>0),
];
$posts_recent =  App\Posts::query_stories($args,'created_at','desc',false);
$post_category = App\Posts::post_category();
$categories =  App\Posts::get_categories_post();
?>
<div class="blog_right_sidebar">
    @if( Illuminate\Support\Facades\Auth::check())
    <aside class="single_sidebar_widget search_widget">
        <h4 class="widget_title"><i class="fa fa-thumb-tack"></i> Perform a Quick Task...</h4>
        <p class="text-left" style="line-height:125%;">Select one of the options below to perform a quick task, such as update your account information, change your password, etc.</p>
        <ul class="text-left sidebar-menu list-unstyled push-top-15">
            <li><i class="fa fa-user black"></i> <a href="#">My Profile</a></li>
            <li><i class="fa fa-pencil black"></i> <a href="#">Update Account Information</a></li>
            <li><i class="fa fa-lock black"></i> <a href="#">Change Your Password</a></li>
            <li><i class="fa fa-plus black"></i> <a href="#" class="disabled-link">Add New Media</a></li>
            <li><i class="fa fa-plus black"></i> <a href="#" class="disabled-link">Add New Story</a></li>
            <li><i class="fa fa-sign-out black"></i> <a href="#" class="">Log Out</a></li>
        </ul>
    </aside>
    @endif
    <aside class="single_sidebar_widget search_widget">
        <form action="#">
            <div class="form-group">
                <div class="input-group mb-3">
                    <input type="text" class="form-control" placeholder="Search Keyword">
                    <div class="input-group-append">
                        <button class="btn" type="button"><i class="ti-search"></i></button>
                    </div>
                </div>
            </div>
            <button type="submit" class="button rounded-0 primary-bg text-white w-100 btn_1">Search</button>
        </form>
    </aside>
    <aside class="single_sidebar_widget post_category_widget">
        <h4 class="widget_title"><i class="fa fa-tags"></i> Categories</h4>
        <ul class="list cat-list">
            @if( isset($categories))
                @foreach($categories as $value)
            <li>
                <a href="#" class="d-flex">
                    <p>{{ $post_category[$value->post_category] }}</p>
                    <p>({{$value->number}})</p>
                </a>
            </li>
                @endforeach
            @endif
        </ul>
    </aside>
    <aside class="single_sidebar_widget popular_post_widget">
        <h3 class="widget_title"><i class="fa fa-book"></i> Recent Stories</h3>
        @if (count($posts_recent) > 0)
            @foreach ($posts_recent as $post)
                <div class="media post_item">
                    @if( isset($post->post_image) )
                        <img src="{{ App\Media::get_media_detail($post->post_image)->link }}" class="img-fluid rounded-lg sidebar-image" alt="post">
                    @endif
                    <div class="media-body">
                        <a href="{{ url('/story/'.$post->slug) }}"><h3>{{ $post->post_title }}</h3></a>
                        <p class="hide">{{ get_current_datetime($post->created_at) }}</p>
                    </div>
                </div>
            @endforeach
        @endif
    </aside>
    <aside class="single_sidebar_widget instagram_feeds">
        <h4 class="widget_title"><i class="fa fa-headphones"></i> Media Feed</h4>
        <ul class="instagram_row flex-wrap">
            <li><a href="#"><img class="img-fluid" src="{{ asset('img/post/post_5.png') }}" alt="image"></a></li>
            <li><a href="#"><img class="img-fluid" src="{{ asset('img/post/post_6.png') }}" alt="image"></a></li>
            <li><a href="#"><img class="img-fluid" src="{{ asset('img/post/post_7.png') }}" alt="image"></a></li>
            <li><a href="#"><img class="img-fluid" src="{{ asset('img/post/post_8.png') }}" alt="image"></a></li>
            <li><a href="#"><img class="img-fluid" src="{{ asset('img/post/post_9.png') }}" alt="image"></a></li>
            <li><a href="#"><img class="img-fluid" src="{{ asset('img/post/post_10.png') }}" alt="image"></a></li>
        </ul>
    </aside>
    <aside class="single_sidebar_widget newsletter_widget">
        <h4 class="widget_title"><i class="fa fa-envelope"></i> Newsletter</h4>
        <form action="#">
            <div class="form-group">
                <input type="email" id="email" name="email" class="form-control" placeholder="Email Address" required="">
            </div>
            <button type="submit" class="button rounded-0 primary-bg text-white w-100 btn_1">Subscribe</button>
        </form>
    </aside>
</div>