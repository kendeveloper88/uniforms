@extends('layouts.layout_main')
@section('content')
    <?php
    $user = Auth::user();
    $faq_d = App\Options::get_option($user->id,'option_faq');
    if($faq_d)$faq = json_decode($faq_d);
    $avata_id = get_user_meta($user->id,'avata');
    if( isset($avata_id) )$avata =  App\Media::get_media_detail($avata_id);
    ?>
    <header class="main_menu single_page_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logos/kindward_logo.png') }}" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                           {!! display_menu() !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="breadcrumb breadcrumb_bg" style="background-image:url('{{ asset('img/backgrounds/background_005.jpg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-info-circle teal"></i><br>My Account</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial_part push-top-25">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p>Account Services</p>
                        <h2 class="teal">My Acount</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="row">
                        <div class="col-12">
                            <div class="job-details-wrapper">
                                <h3 class="teal semibold">Edit Account Information</h3>
                                <p class="push-bottom-25">Update your account by using the form below. When you are done, click "<strong>Save</strong>" to return to your account.</p>
                                <form id="frmEditAccount"   method="post" class="form-light mt-20" role="form">
                                    <fieldset class="bordered-fieldset">
                                        <legend class="teal"><i class="fa fa-info-circle"></i> Account Information</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Username</label>
                                                    <input id="username" type="text" class="form-control" data-type="main" value="{{ $user->username }}" readonly>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Name</label>
                                                    <input id="name" name="name" type="text" data-type="main" class="form-control" value="{{ $user->name }}">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Email</label>
                                                    <input id="email" name="email" type="email" data-type="main" class="form-control" value="{{ $user->email }}">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Password</label>
                                                    <input id="password" name="password" type="password" data-type="main" class="form-control" placeholder="Unchanged" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Address</label>
                                                    <input id="address" name="address" type="text" data-type="meta" class="form-control" value="">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>City</label>
                                                    <input id="city" name="city" type="text" data-type="meta" class="form-control" value="">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>State</label>
                                                    <select id="state" name="state" class="form-control" data-type="meta">
                                                        <option value="0">-- Select --</option>
                                                        <option value="AL">Alabama</option>
                                                        <option value="AK">Alaska</option>
                                                        <option value="AZ">Arizona</option>
                                                        <option value="AR">Arkansas</option>
                                                        <option value="CA">California</option>
                                                        <option value="CO">Colorado</option>
                                                        <option value="CT">Connecticut</option>
                                                        <option value="DC">Washington D.C.</option>
                                                        <option value="DE">Delaware</option>
                                                        <option value="FL">Florida</option>
                                                        <option value="GA">Georgia</option>
                                                        <option value="HI">Hawaii</option>
                                                        <option value="ID">Idaho</option>
                                                        <option value="IL">Illinois</option>
                                                        <option value="IN">Indiana</option>
                                                        <option value="IA">Iowa</option>
                                                        <option value="KS">Kansas</option>
                                                        <option value="KY">Kentucky</option>
                                                        <option value="LA">Louisiana</option>
                                                        <option value="ME">Maine</option>
                                                        <option value="MD">Maryland</option>
                                                        <option value="MA">Massachusetts</option>
                                                        <option value="MI">Michigan</option>
                                                        <option value="MN">Minnesota</option>
                                                        <option value="MS">Mississippi</option>
                                                        <option value="MO">Missouri</option>
                                                        <option value="MT">Montana</option>
                                                        <option value="NE">Nebraska</option>
                                                        <option value="NV">Nevada</option>
                                                        <option value="NH">New Hampshire</option>
                                                        <option value="NJ">New Jersey</option>
                                                        <option value="NM">New Mexico</option>
                                                        <option value="NY">New York</option>
                                                        <option value="NC">North Carolina</option>
                                                        <option value="ND">North Dakota</option>
                                                        <option value="OH">Ohio</option>
                                                        <option value="OK">Oklahoma</option>
                                                        <option value="OR">Oregon</option>
                                                        <option value="PA">Pennsylvania</option>
                                                        <option value="RI">Rhode Island</option>
                                                        <option value="SC">South Carolina</option>
                                                        <option value="SD">South Dakota</option>
                                                        <option value="TN">Tennessee</option>
                                                        <option value="TX" selected="selected">Texas</option>
                                                        <option value="UT">Utah</option>
                                                        <option value="VT">Vermont</option>
                                                        <option value="VA">Virginia</option>
                                                        <option value="WA">Washington</option>
                                                        <option value="WV">West Virginia</option>
                                                        <option value="WI">Wisconsin</option>
                                                        <option value="WY">Wyoming</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Zip Code</label>
                                                    <input id="zipcode" name="zipcode" type="text" class="form-control" value="" data-type="meta">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Phone</label>
                                                    <input id="phone" name="phone" type="text" class="form-control" value="(800) 555-1212" data-type="meta">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label>Gender</label>
                                                    <select id="gender" name="gender" class="form-control" data-type="meta">
                                                        <option value="0" selected="selected">Gender</option>
                                                        <option value="1">Male</option>
                                                        <option value="2">Female</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Bio</label>
                                                    <textarea id="bio" name="bio" class="form-control" rows="4" data-type="meta"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label>Internal Notes</label>
                                                    <textarea id="notes" name="notes" class="form-control" rows="4" data-type="meta"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" onclick="save_account('#frmEditAccount')" class="btn btn-primary float-right"><i class="fa fa-save"></i> Save</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                                <form id="frmEditProfileImage" method="post" class="form-light mt-20 push-top-25" role="form">
                                    <input id="avata" name="avata" type="hidden" value="" data-type="meta">
                                    <fieldset class="bordered-fieldset">
                                        <legend class="teal"><i class="fa fa-image"></i> Current Profile Image</legend>
                                        <div class="row">
                                            <div class="col-md-12 push-bottom-25">
                                                @if( isset($avata) )
                                                <img src="{{ $avata->link }}" class="img-fluid img-rounded" alt="profile image" style="max-width:200px;">
                                                @else
                                                <img src="{{ asset('uploads/users/user.png') }}" class="img-fluid img-rounded" alt="profile image" style="max-width:200px;">
                                                @endif

                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" class="btn btn-primary float-right post-action-link action-edit-avatar" data-toggle="modal" data-target="#MediaModal" ><i class="fa fa-pencil"></i> Update</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                                <form id="frmEditSocialMedia" method="post" class="form-light mt-20 push-top-25 push-bottom-25" role="form">
                                    <fieldset class="bordered-fieldset">
                                        <legend class="teal"><i class="fa fa-group"></i> Social Media Information</legend>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-globe teal"></i> Website</label>
                                                    <input id="website" name="website" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-linkedin teal"></i> LinkedIn Username</label>
                                                    <input id="linkedin" name="linkedin" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-facebook teal"></i> Facebook Username</label>
                                                    <input id="facebook" name="facebook" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-twitter teal"></i> Twitter Username</label>
                                                    <input id="twitter" name="twitter" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-instagram teal"></i> Instagram Username</label>
                                                    <input id="instagram" name="instagram" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-skype teal"></i> Skype Username</label>
                                                    <input id="skype" name="skype" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-snapchat teal"></i> Snapchat Username</label>
                                                    <input id="snapchat" name="snapchat" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="form-group">
                                                    <label><i class="fa fa-youtube teal"></i> YouTube Username</label>
                                                    <input id="youtube" name="youtube" type="text" class="form-control" value=""  data-type="meta">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-12">
                                                <button type="button" id="btnEditProfile" onclick="save_account('#frmEditSocialMedia')" class="btn btn-md btn-primary float-right" ><i class="fa fa-pencil"></i> Update</button>
                                            </div>
                                        </div>
                                    </fieldset>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="blog_right_sidebar">
                        <aside class="single_sidebar_widget search_widget">
                            <h4 class="widget_title teal"><i class="fa fa-thumb-tack"></i> Perform a Quick Task...</h4>
                            <p class="text-left" style="line-height:125%;">Select one of the options below to perform a quick task, such as update your account information, change your password, etc.</p>
                            <ul class="text-left sidebar-menu list-unstyled push-top-15">
                                <li><i class="fa fa-user teal"></i> <a href="http://dev.kindward.com/my-account/" class="teal">My Profile</a></li>
                                <li><i class="fa fa-pencil teal"></i> <a href="http://dev.kindward.com/edit-account/" class="teal">Update Account Information</a></li>
                                <li><i class="fa fa-lock teal"></i> <a href="http://dev.kindward.com/edit-account/" class="teal">Change Your Password</a></li>
                                <li><i class="fa fa-plus teal"></i> <a href="http://dev.kindward.com/" class="teal disabled-link">Add New Media</a></li>
                                <li><i class="fa fa-plus teal"></i> <a href="http://dev.kindward.com/" class="teal disabled-link">Add New Story</a></li>
                                <li><i class="fa fa-sign-out teal"></i> <a href="http://dev.kindward.com/logoff/" class="teal">Log Out</a></li>
                            </ul>
                        </aside>
                    </div>                </div>
            </div>
        </div>
    </section>
@endsection
