@extends('layouts.layout_product')
@section('content')
    <?php
    $cart = Request::session()->get('cart');
    ?>
    <header class="main_menu single_page_menu menu_fixed animated fadeInDown">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logos/kindward_logo.png') }}" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                           {!! display_menu($slug) !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="breadcrumb breadcrumb_bg" style="background-image:url('{{ asset('img/backgrounds/background_006.jpeg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-shopping-cart teal"></i><br>Shopping Cart</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial_part push-top-25 push-bottom-25">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-lg-12">
                    <div class="cart_container">
                        <div class="container shopping-cart-container">
                            <form id="frmShoppingCart"  action="" method="post" class="form-light" enctype="multipart/form-data" role="form">
                                <input type="hidden" id="action" name="action" value="send_to_checkout">
                                <input type="hidden" id="cart_tax_rate" name="cart_tax_rate" value="0.0825">
                                <input type="hidden" id="cart_shipping" name="cart_shipping" value="5.00">
                                <input type="hidden" id="cart_total" name="cart_total" value="35.85">
                                <div class="row">
                                    <div class="col">
                                        <div class="cart_products">
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="cart-title pt-3 pb-2 px-2">
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <h6 class="ml-15">Product</h6>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <h6>Price</h6>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <h6>Quantity</h6>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <h6>Total</h6>
                                                            </div>
                                                            <div class="col-md-1">
                                                                <h6>&nbsp;</h6>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div id="shopping-cart-items">
                                                        @if($cart['products'])
                                                            @foreach($cart['products'] as $key => $item)
                                                        <div class="cart-single-item shopping-cart-item-id-{{ $item['key'] }}" data-item-id="{{ $item['key'] }}">
                                                            <div class="row align-items-center">
                                                                <div class="col-md-6 col-12">
                                                                    <div class="product-item d-flex align-items-center">
                                                                        <a href="{{ $item['link'] }}"><img src="{{ $item['thumbnail'] }}" class="img-fluid shopping-cart-thumbnail" alt="image"></a>
                                                                        <h6><a href="{{ $item['link'] }}">{{ $item['title'] }}</a>
                                                                        <br/>
                                                                        <br/>
                                                                            {{ $item['attributes'] }}
                                                                        </h6>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 col-6">
                                                                    <div class="price">${{ format_currency( floatval($item['subtotal']),2)  }}</div>
                                                                </div>
                                                                <div class="col-md-2 col-6">
                                                                    <div class="quantity-container d-flex align-items-center mt-15">
                                                                        <input type="number" min="0" id="qty-2" name="quantily" data-product-id="{{ $item['key'] }}" class="quantity form-control input-product-qty" onchange="change_quantily(this)" value="{{ $item['quantily'] }}">
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-2 col-12">
                                                                    <button type="button" class="btn btn-link btn-lg btn-remove-product" onclick="remove_product(this)" data-product-id="{{ $item['key'] }}"><i class="fa fa-trash"></i></button>
                                                                </div>
                                                            </div>
                                                        </div>
                                                            @endforeach
                                                            @else
                                                            <div class="product-item d-flex align-items-center">
                                                                <h4 class="pink">There are no items in your shopping cart.</h4>
                                                            </div>
                                                         @endif

                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col">
                                        <div class="cart_control_bar d-flex flex-md-row flex-column align-items-start justify-content-start">
                                            <a type="button" class="button_continue_shopping btn_1 ml-md-auto text-uppercase same-window" href="{{ route('shops') }}">Continue Shopping <i class="fa fa-arrow-right"></i></a>
                                        </div>
                                    </div>
                                </div>
                                <div class="row cart_extra">
                                    <div class="col-lg-6">
                                        <div class="cart_coupon">
                                            <div class="cart_title pb-2">Got a Coupon?</div>
                                            <input disabled="" readonly="" type="text" class="cart_coupon_input" placeholder="Coupon Code">
                                            <button disabled="" type="button" class="button_clear btn_1 text-uppercase"><i class="fa fa-usd"></i> Apply Coupon</button>
                                        </div>
                                    </div>
                                    <div class="col-lg-5 offset-lg-1">
                                        <div class="cart_total">
                                            <div class="cart_title">Cart Total</div>
                                            <ul>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <div class="cart_total_title">Subtotal</div>
                                                    <div class="cart_total_price ml-auto"><span id="shopping-cart-subtotal">$28.50</span></div>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <div class="cart_total_title">Tax</div>
                                                    <div class="cart_total_price ml-auto"><span id="shopping-cart-tax">$2.35</span></div>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <div class="cart_total_title">Shipping</div>
                                                    <div class="cart_total_price ml-auto"><span id="shopping-cart-shipping">$5.00</span></div>
                                                </li>
                                                <li class="d-flex flex-row align-items-center justify-content-start">
                                                    <div class="cart_total_title">Total</div>
                                                    <div class="cart_total_price ml-auto"><span id="shopping-cart-total">$35.85</span></div>
                                                </li>
                                            </ul>
                                            <button type="submit" id="btnCheckout" name="btnCheckout" class="cart_total_button1 btn_1 text-uppercase btn-block push-top-25">Proceed To Checkout <i class="fa fa-arrow-right"></i></button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
