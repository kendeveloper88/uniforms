@extends('layouts.layout_main')
@section('content')
    <?php
    $Category =  App\Posts::post_category();
    $post =  App\Posts::get_post_by_slug($slug);
    $comments = App\Posts::get_comment_post_publish($post->id);
    ?>
    <header class="main_menu single_page_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/')  }}"><img src="https://www.kindward.com/img/logos/kindward_logo.png" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                            {!! display_menu($slug) !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-book teal"></i><br>{{ $post->post_title }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blog_area single-post-area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 posts-list">
                    <div class="single-post">
                        <div class="feature-img">
                            @if( isset($post->post_image) )
                            <img src="{{ App\Media::get_media_detail($post->post_image)->link }}" class="img-fluid rounded-lg" alt="story image">
                            @endif
                        </div>
                        <div class="blog_details">
                            <h2>{{ $post->post_title }}</h2>
                            <ul class="blog-info-link mt-3 mb-4">
                                <li><a href="" class="disabled-link"><i class="fa fa-user"></i> {{ App\User::getUserByID($post->user_id)->name }}</a></li>
                                <li><a href="" class="disabled-link"><i class="fa fa-tags"></i> {{ $Category[$post->post_category]}}</a></li>
                                <li><a href="" class="disabled-link"><i class="fa fa-comments"></i> {{ $comments->total() }} Comments</a></li>
                                <li><a href="" class="action-send-digital-gift " title="Login to send a gift"><i class="fa fa-gift green"></i> Send Gift</a></li>
                            </ul>
                            {!! $post->post_content !!}
                            <div class="text-center pad-top-5">
                                @if( get_meta_post ($post->id,'audio') )
                                    <audio controls=""><source src="{{  App\Media::get_media_detail(get_meta_post ($post->id,'audio'))->link }}" type="audio/mpeg"></audio>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="navigation-top">
                        <div class="d-sm-flex justify-content-between text-center">
                            <p class="like-info"><span class="align-middle"><i class="fa fa-heart red"></i></span> 4 people like this</p>
                            <div class="col-sm-4 text-center my-2 my-sm-0">
                                <p class="comment-count"><span class="align-middle"><i class="fa fa-comment"></i></span> {{ $comments->total() }} Comments</p>
                            </div>
                            <ul class="social-icons">
                                <li><a href="" title="Facebook" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                <li><a href="" title="Twitter" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                <li><a href="" title="Instagram" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                <li><a href="" title="YouTube" target="_blank"><i class="fa fa-youtube"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="blog-author">
                        <div class="media align-items-center">
                            <img src="{{ asset('uploads/users/1560194955.png') }}" alt="image">
                            <div class="media-body">
                                <a href="#"><h4>{{ App\User::getUserByID($post->user_id)->name }}</h4></a>
                                <p>Moving kindness forward.</p>
                            </div>
                        </div>
                    </div>
                    <div class="comments-area">
                        <h4><i class="fa fa-comments-o"></i> {{ $comments->total() }} Comments</h4>
                        @if (count($comments) > 0)
                            @foreach ($comments as $comment)
                        <div class="comment-list">
                            <div class="single-comment justify-content-between d-flex">
                                <div class="user justify-content-between d-flex">
                                    <div class="thumb">
                                        <img src="{{ asset('uploads/users/user.png') }}" alt="image">
                                    </div>
                                    <div class="desc" style="width:100%;">
                                        <p class="comment">{{ $comment->content }}</p>
                                        <div class="d-flex justify-content-between">
                                            <div class="d-flex align-items-center">
                                                <h5><a href="#">{{ App\User::getUserByID($comment->user_id)->name }}</a></h5>
                                                <p class="date">{{ get_current_datetime($comment->created_at) }}</p>
                                            </div>
                                            <div class="reply-btn hide">
                                                <button type="button" id="btnReplyComment" class="btn btn-sm btn-orange text-uppercase">Reply</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                            @endforeach
                        @endif
                    </div>
                    <div class="row">
                        <div class="col-lg-12 push-top-25 bg_cornsilk img-rounded pad-top-25 pad-bottom-25">
                            <div class="comment-form-wrap">
                                <h3 class="semibold"><i class="fa fa-comments"></i> Leave A Comment</h3>
                                @if( \Illuminate\Support\Facades\Auth::check() )
                                <form id="frmAddStoryComment"  method="post" action="{{ url('/story/'.$post->slug) }}" class="bg_cornsilk">
                                    <input type="hidden" id="post_id" name="post_id" value="{{ $post->id }}">
                                    <input type="hidden" id="post_slug" name="slug" value="{{ $post->slug }}">
                                    <input type="hidden" id="user_id" name="user_id" value="{{ Illuminate\Support\Facades\Auth::id() }}">
                                    <input type="hidden"  name="action" value="add_comment_post">
                                    {{ csrf_field() }}
                                    <div class="form-group">
                                        <label for="post_comments">Comments:</label>
                                        <textarea id="post_comments" name="content" cols="30" rows="5" class="form-control"></textarea>
                                    </div>
                                    <div class="form-group">
                                        <button type="button" id="btnSubmit" class="button btn_commentForm button-contactForm float-right"><i class="fa fa-send"></i> Post Comment <span class="d-none">
                                <i class="fas fa-spinner fa-spin"></i>
                            </span></button>
                                    </div>
                                </form>
                                @else
                                    <div class="callout callout-danger">
                                        <p>You need <a href="{{ url('login') }}"><strong>Sign in</strong></a> to comments.</p>
                                    </div>
                                @endif
                            </div>
                        </div>
                    </div>

                </div>
                <div class="col-lg-4">
                    @include('frontend.sidebar_right')
                </div>
            </div>
        </div>
    </section>
@endsection
