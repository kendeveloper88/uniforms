@extends('layouts.layout_product')
@section('content')
    <?php
    $cart = Request::session()->get('cart');
    $product = App\Product::get_product_bySlug($slug);
    $product_categories =  App\Product::get_product_categories();
    $product_type = App\Product::product_type();
    $product_attributes =  App\Product::product_attributes();
    // check product if fail
    if($product){
        if($product->featured_image){
            $featured_image = App\Media::get_media_detail($product->featured_image)->link;
        }
        $category = App\Relationships::get_relationships($product->id,'product');
        $term_id = $category ? $category->term_id:'';
        $price = App\Product::get_meta_product($product->id,'price');
        $shipping = App\Product::get_meta_product($product->id,'shipping');
        $gallery = App\Product::get_meta_product($product->id,'gallery');
        if($gallery)$galleries = \GuzzleHttp\json_decode($gallery);
        $size = App\Product::get_meta_product($product->id,'size');
        if($size)$size = \GuzzleHttp\json_decode($size);
        $color = App\Product::get_meta_product($product->id,'color');
        if($color)$color = \GuzzleHttp\json_decode($color);
        $additional_information = App\Product::get_meta_product($product->id,'additional_information');
        if($additional_information)$additional_informations = \GuzzleHttp\json_decode($additional_information);
    }

    ?>
    <header class="main_menu single_page_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logos/kindward_logo.png') }}" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                            {!! display_menu('gift-shop') !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    @if(isset($product))
    <section class="breadcrumb breadcrumb_bg" style="background-image:url('{{ asset('img/backgrounds/background_013.jpg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-shopping-cart teal"></i><br>{{ $product->name }}</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="course_details_area pad-top-25">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 course_details_left">
                    <div class="main_image">
                        <img id="xzoom-default" src="{{ $featured_image }}" xoriginal="{{ $featured_image }}" class="img-fluid xzoom" alt="image" style="width: 100%;">
                    </div>
                    <div class="row">
                        <div class="col-md-12 push-top-25">
                            <div class="xzoom-thumbs">
                                @if(isset($galleries))
                                    @foreach($galleries as $value)
                                <a href="{{ $value->link }}"><img class="xzoom-gallery xactive" width="80px" src="{{ $value->link }}"></a>
                                    @endforeach
                                 @endif
                            </div>
                        </div>
                    </div>
                    <div class="content_wrapper">
                        <h4 class="title_top"><i class="fa fa-shopping-bag"></i> Product Description</h4>
                        <div class="content table-responsive">
                           {!! $product->description !!}
                        </div>
                        <h4 class="title"><i class="fa fa-info-circle"></i> Additional Information</h4>
                        <div class="content">
                            <table class="table table-striped">
                                <thead></thead>
                                <tbody>
                                @if(isset($additional_informations))
                                    @foreach($additional_informations as $item)
                                        <!---show title-->
                                        @if( $item->name == 'title')
                                <tr>
                                    <td>{{ $item->value }}</td>
                                @endif
                                <!--show content-->
                                    @if($item->name == 'content')
                                    <td>{{ $item->value }}</td>
                                </tr>
                                        @endif
                                    @endforeach
                                @endif

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4 right-contents">
                    <div class="sidebar_top detail-single-product">
                        <h3 class="semibold"><i class="fa fa-info-circle"></i> Quick Details</h3>
                        <ul>
                            <li><a class="justify-content-between d-flex" href="" onclick="return false;">
                                    <p>Category</p>
                                    <span class="color">{{ display_category_product($product->id)  }}</span>
                                </a>
                            </li>
                            <li><a class="justify-content-between d-flex" href="" onclick="return false;">
                                    <p>Stock #</p>
                                    <span class="color">{{ time() }}</span>
                                </a>
                            </li>
                            <li><a class="justify-content-between d-flex" href="" onclick="return false;">
                                    <p>Price</p>
                                    <span>${{ format_currency($price) }}</span>
                                </a>
                            </li>
                            <li><a class="justify-content-between d-flex" href="" onclick="return false;">
                                    <p>Shipping</p>
                                    <span>${{ format_currency($shipping) }}</span>
                                </a>
                            </li>
                            <!--Attributes-->
                            <li><a class="justify-content-between d-flex" href="" onclick="return false;">
                                    <p>Color</p>
                                    <span>
                                         <select class="select2bs4 form-control"  name="color" data-placeholder="Select a Color" data-title="Color" >
                                                @foreach($color as  $value)
                                                 <option value="{{ $value }}">{{$product_attributes['color']['value'][$value] }}</option>
                                             @endforeach
                                            </select>
                                    </span>
                                </a>
                            </li>
                            <li><a class="justify-content-between d-flex" href="" onclick="return false;">
                                    <p>Size</p>
                                    <span>
                                                <select class="select2bs4 form-control"  name="size" data-placeholder="Select a size" data-title="size" >
                                                @foreach($size as  $value)
                                                        <option value="{{ $value }}">{{$product_attributes['size']['value'][$value] }}</option>
                                                    @endforeach
                                            </select>
                                            </span>
                                </a>
                            </li>


                            <li><a class="justify-content-between d-flex" href="" onclick="return false;">
                                    <p>Quantity</p>
                                    <span><input type="text" id="qty" name="quantily" class="form-control ml-15 quantity" value="1"></span>
                                </a>
                            </li>
                        </ul>
                        <input type="hidden" name="product_id" value="{{ $product->id }}"  />
                        <input type="hidden" name="title" value="{{ $product->name }}"  />
                        <input type="hidden" name="thumbnail" value="{{ $featured_image }}"  />
                        <input type="hidden" name="subtotal" value="{{ $price }}"  />
                        <input type="hidden" name="link" value="{{ url('/product/'.$product->slug) }}"  />
                        <button type="button" class="btn_1 btn-block white btn-add-to-cart" onclick="add_cart()" data-product-name="Harry Styles Graphic Tees Causal Treat People with Kindness T-shirt Women Be A Lover Tops Love Everyone Valentine's Day T Shirt" data-product-price="12.14" data-product-image="https://www.kindward.com/uploads/products/3/1551851295.jpg"><i class="fa fa-shopping-cart"></i> Add to Cart</button>
                    </div>
                    <h4 class="title"><i class="fa fa-star"></i> Reviews</h4>
                    <div class="content">
                        <div class="review-top row pt-40">
                            <div class="col-lg-12">
                                <h6 class="mb-15">Review This Product</h6>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Quality</span>
                                    <div class="rating">
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <span>Outstanding</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Puncuality</span>
                                    <div class="rating">
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <span>Outstanding</span>
                                </div>
                                <div class="d-flex flex-row reviews justify-content-between">
                                    <span>Quality</span>
                                    <div class="rating">
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                        <a href="#"><img src="https://www.kindward.com/img/icon/star.svg" alt=""></a>
                                    </div>
                                    <span>Outstanding</span>
                                </div>
                            </div>
                        </div>
                        <div class="feedeback">
                            <h6>Your Feedback</h6>
                            <textarea name="feedback" class="form-control" cols="10" rows="10"></textarea>
                            <div class="mt-10 text-right">
                                <a href="" onclick="return false;" class="btn_1"><i class="fa fa-send"></i> Submit</a>
                            </div>
                        </div>
                        <div class="comments-area mb-30">
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="https://www.kindward.com/img/cource/cource_1.png" alt="image">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Matthew Speck</a></h5>
                                            <div class="rating">
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">Great product!!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="https://www.kindward.com/img/cource/cource_2.png" alt="image">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Stephen Williams</a></h5>
                                            <div class="rating">
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">Product received was exactly as described. Love it!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="comment-list">
                                <div class="single-comment single-reviews justify-content-between d-flex">
                                    <div class="user justify-content-between d-flex">
                                        <div class="thumb">
                                            <img src="https://www.kindward.com/img/cource/cource_3.png" alt="image">
                                        </div>
                                        <div class="desc">
                                            <h5><a href="#">Maria Ledowski</a></h5>
                                            <div class="rating">
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/color_star.svg" alt=""></a>
                                                <a href="" onclick="return false;"><img src="https://www.kindward.com/img/icon/star.svg" alt=""></a>
                                            </div>
                                            <p class="comment">I ordered the hoodie for those chilly nights. I couldn't be happier with it!</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @endif
@endsection
