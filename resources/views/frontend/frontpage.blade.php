@extends('layouts.layout_main')
@section('content')
    <?php
   $top_category = [
       [
           'title'=>'Security & law enforcament',
           'item'=>3595,
           'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
           'url'=>'#',
           'image'=>'images/category/category1.png',
       ],

       [
           'title'=>'Fire Rescue & Retardent',
           'item'=>149,
           'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
           'url'=>'#',
           'image'=>'images/category/category2.png',
       ],
       [
           'title'=>'Transportation',
           'item'=>1328,
           'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
           'url'=>'#',
           'image'=>'images/category/category3.png',
       ],
       [
           'title'=>'Aviation & Airlines',
           'item'=>92,
           'description'=>'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua',
           'url'=>'#',
           'image'=>'images/category/category4.png',
       ],
   ];

   $top_products = [
       [
           'sku'=>'8800l',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product1.png',
       ],
       [
           'sku'=>'8800S',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Short Sleeve Shirt',
           'price'=>12.05,
           'image'=>'images/products/product2.png',
       ],
       [
           'sku'=>'8FC-TT02S',
           'category'=>'National Patrol',
           'title'=>'Two Tone Bike Patrol Shirt - Zipper Pocket - Short Sleeve',
           'price'=>24.00,
           'image'=>'images/products/product3.png',
       ],
       [
           'sku'=>'3000',
           'category'=>'National Patrol',
           'title'=>"Men's Security Polyester/Twill Pants",
           'price'=>12.90,
           'image'=>'images/products/product4.png',
       ],
       [
           'sku'=>'1400',
           'category'=>'National Patrol',
           'title'=>"Men's Polyester Single Breasted Blazer",
           'price'=>12.95,
           'image'=>'images/products/product5.png',
       ],
       [
           'sku'=>'5000',
           'category'=>'National Patrol',
           'title'=>"Men's Polyester Single Breasted Blazer",
           'price'=>12.95,
           'image'=>'images/products/product6.png',
       ],
       [
           'sku'=>'1000',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product7.png',
       ],
       [
           'sku'=>'6RT-5063',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product8.png',
       ],
       [
           'sku'=>'6RT-5005',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product9.png',
       ],
       [
           'sku'=>'6RT-5052',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product10.png',
       ],

       [
           'sku'=>'8800l',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product1.png',
       ],
       [
           'sku'=>'8800S',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Short Sleeve Shirt',
           'price'=>12.05,
           'image'=>'images/products/product2.png',
       ],
       [
           'sku'=>'8FC-TT02S',
           'category'=>'National Patrol',
           'title'=>'Two Tone Bike Patrol Shirt - Zipper Pocket - Short Sleeve',
           'price'=>24.00,
           'image'=>'images/products/product3.png',
       ],
       [
           'sku'=>'3000',
           'category'=>'National Patrol',
           'title'=>"Men's Security Polyester/Twill Pants",
           'price'=>12.90,
           'image'=>'images/products/product4.png',
       ],
       [
           'sku'=>'1400',
           'category'=>'National Patrol',
           'title'=>"Men's Polyester Single Breasted Blazer",
           'price'=>12.95,
           'image'=>'images/products/product5.png',
       ],
       [
           'sku'=>'5000',
           'category'=>'National Patrol',
           'title'=>"Men's Polyester Single Breasted Blazer",
           'price'=>12.95,
           'image'=>'images/products/product6.png',
       ],
       [
           'sku'=>'1000',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product7.png',
       ],
       [
           'sku'=>'6RT-5063',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product8.png',
       ],
       [
           'sku'=>'6RT-5005',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product9.png',
       ],
       [
           'sku'=>'6RT-5052',
           'category'=>'National Patrol',
           'title'=>'Mens 100% Polyester Security Shirt Long Sleeve',
           'price'=>12.95,
           'image'=>'images/products/product10.png',
       ],

   ]
    ?>
    <section class="banner_part">
        <div id="CarouselHome" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <div class="carousel-item active" style="background-image: url({{ url('images/slider1.jpg') }})">
                    <dic class="content-slider">
                        <div class="dataContent">
                            <h2>Dual east & west coast</h2>
                            <label>Distribution centers</label>
                            <ul>
                               <li><span>Law enforcement & security</span></li>
                               <li><span>Aviation & parking</span></li>
                               <li><span>Fire rescue & Retardent</span></li>
                               <li><span>Executive & career apparel</span></li>
                               <li><span>Transportation</span></li>
                               <li><span>Work & industrial</span></li>
                            </ul>
                            <a class="btn-button">Shop now</a>
                        </div>
                        <div class="border-content"></div>
                    </dic>
                    <img class="d-block w-100" src="{{ url('images/slider1.jpg') }}" alt="First slide">
                </div>
                <div class="carousel-item" style="background-image: url({{ url('images/slider1.jpg') }})">
                    <dic class="content-slider">
                        <div class="dataContent">
                            <h2>Dual east & west coast</h2>
                            <label>Distribution centers</label>
                            <ul>
                                <li><span>Law enforcement & security</span></li>
                                <li><span>Aviation & parking</span></li>
                                <li><span>Fire rescue & Retardent</span></li>
                                <li><span>Executive & career apparel</span></li>
                                <li><span>Transportation</span></li>
                                <li><span>Work & industrial</span></li>
                            </ul>
                            <a class="btn-button">Shop now</a>
                        </div>
                        <div class="border-content"></div>
                    </dic>
                    <img class="d-block w-100" src="{{ url('images/slider1.jpg') }}" alt="Second slide">
                </div>
            </div>
            <a class="carousel-control-prev" href="#CarouselHome" role="button" data-slide="prev">
                <span class="carousel-icon carousel-control-prev-icon" aria-hidden="true"></span>
            </a>
            <a class="carousel-control-next" href="#CarouselHome" role="button" data-slide="next">
                <span class="carousel-icon carousel-control-next-icon" aria-hidden="true"></span>
            </a>
        </div>

    </section>
    <section id="partners" class="mt-5 mb-5">
        <div class="container">
            <div class="row text-center">
                <div class="col-md-2"><a><img src="{{ url('images/partner/partner1.png')  }}"/></a></div>
                <div class="col-md-2"><a><img src="{{ url('images/partner/partner2.png')  }}"/></a></div>
                <div class="col-md-2"><a><img src="{{ url('images/partner/partner3.png')  }}"/></a></div>
                <div class="col-md-2"><a><img src="{{ url('images/partner/partner4.png')  }}"/></a></div>
                <div class="col-md-2"><a><img src="{{ url('images/partner/partner5.png')  }}"/></a></div>
                <div class="col-md-2"><a><img src="{{ url('images/partner/partner6.png')  }}"/></a></div>
            </div>
        </div>
    </section>
    <section class="Top-Category">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <h2 class="">Top Category</h2>
                    </div>
                </div>
            </div>
            <div class="row">
                @if($top_category)
                    @foreach($top_category as $item)
                <div class="Item-Category col-md-3" style="background-image: url( {{ url($item['image']) }} )">
                    <div class="ContentItem">
                        <div class="title">{{  $item['title'] }}</div>
                        <div class="numberItem">{{ $item['item']  }} items</div>
                        <div class="description">{{ $item['description']  }}</div>
                        <a href="{{ $item['url'] }}" class="btn-button">View more</a>
                    </div>
                </div>
                    @endforeach
                 @endif
            </div>
        </div>
    </section>
    <section class="Top-Product mt-5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <h2 class="">Top Item</h2>
                    </div>
                    <ul class="list-product text-center">
                        <li class="d-inline-block"><a>New Arrival</a></li>
                        <li class="d-inline-block"><a class="active">Best seller</a></li>
                        <li class="d-inline-block"><a>Special Product</a></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="container-fluid">
            <div id="ItemProductSlider" class="owl-carousel owl-theme">
                @foreach($top_products as $key=>$item)
                @if($key%2)
                        <div class="item-product">{!! showItemProduct($item) !!}</div>
                        <?= '</div>' ?>
                        @else
                        <?= '<div class="ItemSlider">' ?>
                        <div class="item-product">{!! showItemProduct($item) !!}</div>
                    @endif
                @endforeach
            </div>
        </div>
    </section>

    <section class="Our-Newsletter mt-5 pt-5 pb-5">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <h2 class="">Our newsletter</h2>
                    </div>
                   <div class="section_description text-center">Sign up for unipro news, sales and deals</div>
                    <form class="form-newsletter">
                        <div class="form-group text-center d-flex">
                            <input type="email" placeholder="Email address" required>
                            <button type="button">Send</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

@endsection
