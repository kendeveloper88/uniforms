@extends('layouts.layout_main')
@section('content')
    <?php
    $args = [
        array('name'=>'post_type','operator'=>'=','value'=>0),
    ];
    $posts =  App\Posts::query_stories($args,'updated_at','asc',20);
    ?>
    <header class="main_menu single_page_menu">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/')  }}"><img src="https://www.kindward.com/img/logos/kindward_logo.png" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                            {!! display_menu($slug) !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="breadcrumb breadcrumb_bg">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-book teal"></i><br>Our Stories</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="blog_area section_padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
                        <div class="row">
                            @if (count($posts) > 0)
                                @foreach ($posts as $post)
                            <div class="col-md-6">
                                <article class="blog_item">
                                    <div class="blog_item_img">
                                        @if( isset($post->post_image) )
                                            <img class="card-img img-fluid rounded-lg" src="{{ App\Media::get_media_detail($post->post_image)->link }}" alt="image">
                                        @endif
                                    </div>
                                    <div class="blog_details">
                                        <a class="d-inline-block" href="{{ url('/post/'.$post->slug) }}"><h2>{{ $post->post_title }}</h2></a>
                                        {!! limit_text($post->post_content,15) !!}
                                        <ul class="blog-info-link">
                                            <li><a href="" onclick="return false;"><i class="fa fa-user"></i> {{ App\User::getUserByID($post->user_id)->name }}</a></li>
                                            <li><a href="" onclick="return false;"><i class="fa fa-comments"></i> {{ App\Posts::get_comment_post($post->id)->total() }} Comments</a></li>
                                        </ul>
                                    </div>
                                </article>
                            </div>
                                @endforeach
                            @endif
                        </div>
                        @if($posts->hasMorePages())
                        <nav class="blog-pagination justify-content-center d-flex">
                            <ul class="pagination">
                                @if( $posts->previousPageUrl())
                                <li class="page-item"><a href="{{ $posts->previousPageUrl() }}" class="page-link" aria-label="Previous"><i class="fa fa-arrow-left"></i></a></li>
                                @endif
                                <li class="page-item {{ $posts->currentPage()?'active':'' }}"><a class="page-link">{{ $posts->currentPage() }}</a></li>
                                @if( $posts->nextPageUrl())
                                <li class="page-item"><a href="{{ $posts->nextPageUrl() }}" class="page-link" aria-label="Next"><i class="fa fa-arrow-right"></i></a></li>
                                @endif
                            </ul>
                        </nav>
                            @endif
                    </div>
                </div>
                <div class="col-lg-4">
                    @include('frontend.sidebar_right');
                </div>
            </div>
        </div>
    </section>
@endsection
