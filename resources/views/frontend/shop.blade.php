@extends('layouts.layout_main')
@section('content')
    <?php
    $product_categories =  App\Product::get_product_categories();
    $products =  App\Product::get_products();
    $product_status = App\Product::product_status();
    ?>
    <header class="main_menu single_page_menu menu_fixed animated fadeInDown">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logos/kindward_logo.png') }}" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                            {!! display_menu($slug) !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="breadcrumb breadcrumb_bg" style="background-image:url('{{ asset('img/backgrounds/background_013.jpg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-gift teal"></i><br>Our Gift Shop</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="testimonial_part section_padding">
        <div class="container-fluid">
            <div class="row justify-content-center">
                <div class="col-xl-5">
                    <div class="section_tittle text-center">
                        <p>Shop Our Online Gift Shop</p>
                        <h2 class="teal">Make Someone Smile</h2>
                    </div>
                </div>
            </div>
            <div class="row justify-content-center">
                <div class="col-lg-8">
                    <div class="filter-wrapper text-center">
                        <button class="btn btn-info filter-button" data-filter="all">All</button>
                        @if($product_categories)
                            @foreach($product_categories as $category)
                        <button class="btn btn-info filter-button" data-filter="{{ $category->slug }}">{{ $category->name }}</button>
                            @endforeach
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                @if (count($products) > 0)
                    @foreach ($products as $product)
                <div class="col-sm-6 col-lg-4 filter {{ display_category_product($product->id,'slug')  }}">
                    <div class="single_special_cource push-bottom-50 px-2 py-2 text-center rounded-lg" style="border:1px solid #12B8C1;">
                        @if( isset($product->featured_image) )
                        <img src="{{ App\Media::get_media_detail($product->featured_image)->link }}" class="img-fluid mx-auto" alt="image" style="max-width:360px; max-height:313px;">
                        @endif
                        <div class="special_cource_text">
                            <a href="{{ url('/product/'.$product->slug) }}" class="push-top-5 btn btn-md btn-teal">${{ App\Product::get_meta_product($product->id,'price') }} <i class="fa fa-arrow-right"></i></a>
                            <a href="{{ url('/product/'.$product->slug) }}" title="{{ $product->name }}"><h4 class="push-top-5">{{ limit_text($product->name,6) }}</h4></a>
                        </div>
                    </div>
                </div>
                    @endforeach
                @endif

            </div>
        </div>
    </section>
@endsection
