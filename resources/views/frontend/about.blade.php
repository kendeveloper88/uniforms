@extends('layouts.layout_main')
@section('content')
    <?php
    $faq_title = $faq = App\Options::list_faq();
    $user_id = Auth::id();
    $faq_d = App\Options::get_option($user_id,'option_faq');
    if($faq_d)$faq = json_decode($faq_d);
    ?>
    <header class="main_menu single_page_menu menu_fixed animated fadeInDown">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-12">
                    <nav class="navbar navbar-expand-lg navbar-light">
                        <a class="navbar-brand" href="{{ url('/') }}"><img src="{{ asset('img/logos/kindward_logo.png') }}" alt="logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
                        <div class="collapse navbar-collapse main-menu-item justify-content-end" id="navbarSupportedContent">
                            {!! display_menu($slug) !!}
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </header>
    <section class="breadcrumb breadcrumb_bg" style="background-image:url('{{ asset('img/backgrounds/background_006.jpeg') }}');">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="breadcrumb_iner text-center">
                        <div class="breadcrumb_iner_item">
                            <h2><i class="fa fa-info-circle teal"></i><br>About Us</h2>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="feature_part">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-xl-3 align-self-center">
                    <div class="single_feature_text ">
                        <h2>Kindward Empowers</h2>
                        <p>There are several kind things that you can do for people. Here are some ideas to get you started, and how our system works.</p>
                        <img src="{{ asset('img/orange_shooting_star_001.png') }}" class="img-fluid" alt="image">
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><img src="{{ asset('img/rsz_connections.png') }}" class="img-fluid" alt="image"></span>
                            <h4>Join the Movement</h4>
                            <p>We need your help in spreading kindness around the world. Become the better you &amp; make a difference. Connect &amp; contribute with acts of kindness. Send love and good vibes to the world.</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part">
                            <span class="single_feature_icon"><img src="{{ asset('img/man-with-speech-bubble.png') }}" class="img-fluid" alt="image"></span>
                            <h4>Post Your Story</h4>
                            <p>Share stories of kindness - performed, seen or imagined. Kindward acts that will drive the engine of positive change. Seen something unkind? Share your solution!</p>
                        </div>
                    </div>
                </div>
                <div class="col-sm-6 col-xl-3">
                    <div class="single_feature">
                        <div class="single_feature_part single_feature_part_2">
                            <span class="single_service_icon style_icon"><img src="{{ asset('img/gift_7.png') }}" class="img-fluid" alt="image"></span>
                            <h4>Pay It Forward</h4>
                            <p>Collect K-notes that empower you to send free, cool gifts to your family, friends, colleagues or somebody you never met. Bring a smile to that unsuspecting face.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="learning_part">
        <div class="container">
            <div class="row align-items-sm-center align-items-lg-stretch">
                <div class="col-md-7 col-lg-7">
                    <div class="learning_img">
                        <img src="{{ asset('img/kindness-clipart-001.png') }}" class="img-fluid" alt="image">
                    </div>
                </div>
                <div class="col-md-5 col-lg-5">
                    <div class="learning_member_text">
                        <h5>About Us</h5>
                        <h2>Moving Kindness Forward</h2>
                        <p>Welcome to the Kindward community. You’ve arrived at a different type of social networking site. Our hope is that Kindward will move kindness forward – in our homes, schools, workplaces and communities. A kinder community leads to a kinder planet!</p>
                        <p>By sharing acts of kindness on our Kindward site, you are promoting and inspiring the many ways to show kindness and give back. You are contributing to a more benevolent world; a world focused on others. Our gifting program is one more way to support our pay it forward philosophy. Forwarding a gift helps spread kindness to those close to you, to new friends or to those in need. Start sharing your acts of kindness now.</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
