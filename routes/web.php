<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('frontend.frontpage',['slug'=>'/']);
});
Auth::routes();
Route::get('/admin', function(){
    return view('admin.dashboard');
})->name('admin')->middleware('auth')->middleware(['role:administrator']);
Route::get('/home', function(){
    return view('home');
});
//Login
Route::post('loginsite','HomeController@check');

//Group admin
Route::group(['prefix' => 'admin','as'=>'admin.','middleware' => ['auth','role:administrator']], function () {

//    Display users
    Route::get('users', function () {
        $roles = App\User::get_roles();
        return view('admin.users.users',['roles'=>$roles]);
    })->name('users');

//    Display profile user
    Route::get('profile', function () {
        $roles = App\User::get_roles();
        return view('admin.users.profile',['roles'=>$roles]);
    })->name('profile');

    //    Display profile user other
    Route::get('user/{id}/view', function ($id) {
        $roles = App\User::get_roles();
        $user = App\User::getUserByID($id);
        return view('admin.users.user_view',['user'=>$user,'roles'=>$roles]);
    })->name('user_view');

    //    Edit profile user other
    Route::get('user/{id}/edit', function ($id) {
        return view('admin.users.user_edit',['user_id'=>$id]);
    })->name('user_edit');
// change passord
    Route::post('change_password', 'auth\UpdateUser@ChangePassword');
    // update field main user
    Route::post('update_userinfo', 'auth\UpdateUser@UpdateUserInfo');
    // ajax delete user
    Route::post('detele_user', 'auth\UpdateUser@DeleteUser');

    // Posts
    Route::get('posts', function () {
        return view('admin.posts.posts');
    })->name('posts');
    // add new story
    Route::get('add-post', function () {
        return view('admin.posts.addnew_post');
    })->name('add-post');

    // edit story
    Route::get('post/{id}/edit', function ($id) {
        return view('admin.posts.edit_post',['post_id'=>$id]);
    })->name('edit_post');
    // add story
    Route::post('add_post', 'ControllerPosts@add_post');
    //save story
    Route::post('save_post', 'ControllerPosts@save_post');

    // ajax story & all
    Route::post('admin_ajax', 'ControllerAjax@admin_ajax');

    // Media
    Route::get('media', function () {
        return view('admin.media.media');
    })->name('media');
    
    Route::get('media/list', function () {
        return view('admin.media.media_list');
    })->name('media_list');

    Route::post('upload','ControllerMedia@doUpload');

    //products
    Route::get('products', function () {
        return view('admin.products.products');
    })->name('products');

    // product categories
    Route::get('product_categories', function () {
        return view('admin.products.product_categories');
    })->name('product_categories');
    // add product categories
    Route::POST('add_product_category', 'ControllerProduct@add_product_category');

    // add new product
    Route::get('add-product', function () {
        return view('admin.products.add-product');
    })->name('add-product');

    Route::post('add_product','ControllerProduct@add_product');

    // edit product
    Route::get('product/{id}/edit', function ($id) {
        return view('admin.products.edit-product',['product_id'=>$id]);
    })->name('edit-product');

    Route::post('edit_product','ControllerProduct@save_product');

    //Orders
    Route::get('orders', function () {
        return view('admin.order.orders');
    })->name('orders');

    // edit product
    Route::get('order/{id}/view', function ($id) {
        return view('admin.order.view-order',['order_id'=>$id]);
    })->name('view-order');

    // edit new orders
    Route::get('order/{id}/edit', function ($id) {
        return view('admin.order.edit-order',['order_id'=>$id]);
    })->name('edit-order');
    // edit order
    Route::post('edit_order','ControllerOrders@save_order');

    // FAQ setting

    // edit new orders
    Route::get('faq', function () {
        return view('admin.widgets.faq');
    })->name('faq');
    // update FAQ
    Route::post('save_faq','ControllerOptions@save_faq');

    // Contact Form
    Route::get('contact-form', function () {
        return view('admin.widgets.contact-form');
    })->name('contact-form');

    Route::get('contact-form/{slug}', function ($slug) {
        return view('admin.widgets.ctf-detail',['slug'=>$slug]);
    })->name('contact-form-detail');


    // Data Form
    Route::get('data-form', function () {
        return view('admin.maketing.data-form');
    })->name('data-form');

    // add data form
    Route::get('add-data-form', function () {
        return view('admin.maketing.add_data-form');
    })->name('add-data-form');

    // edit data form
    Route::get('data-form/{id}/edit', function ($id) {
        return view('admin.maketing.edit_data-form',['id'=>$id]);
    })->name('edit-data-form');



    // System Settings
    Route::get('system-settings', function () {
        return view('admin.system-settings');
    })->name('system-settings');
});


// main pages
// Stories page
Route::get('/posts', function(){
    return view('frontend.posts',['slug'=>'posts']);
});

// Story detail
Route::get('/post/{slug}', function($slug){
    return view('frontend.single-post',['slug'=>$slug]);
});

//Add story comments
Route::post('/post/{slug}','ControllerStories@add_post_comment');

// Media page
Route::get('/media', function(){
    return view('frontend.media',['slug'=>'media']);
});

// FAQ page
Route::get('/faq', function(){
    return view('frontend.faq',['slug'=>'faq']);
});

// About page
Route::get('/about', function(){
    return view('frontend.about',['slug'=>'about']);
});

// Shop page
Route::get('/gift-shop', function(){
    return view('frontend.shop',['slug'=>'gift-shop']);
})->name('shops');

// Story detail
Route::get('/product/{slug}', function($slug){
    return view('frontend.single-product',['slug'=>$slug]);
});


// My account
Route::get('/my-account', function(){
    return view('frontend.account',['slug'=>'account']);
})->name('my-account')->middleware('auth');

// ajax not admin all
Route::post('nopriv_ajax', 'ControllerAjax@admin_ajax')->name('nopriv_ajax')->middleware('auth');
Route::post('nopriv_upload','ControllerMedia@doUpload')->name('nopriv_ajax')->middleware('auth');


// My cart
Route::get('/cart', function(){
    return view('frontend.cart',['slug'=>'cart']);
})->name('cart');