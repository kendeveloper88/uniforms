$(function () {
    if( document.querySelector('.owl-carousel') ) {
        $(".owl-carousel").owlCarousel({
            nav: true,
            dots: false,
            loop: false,
            rewind: true,
            // autoWidth: true,
            stagePadding: 0,
            margin: 0,
            checkVisibility: true,
            navElement: 'div',
            responsive : {
                0 : {
                    items: 1,
                    slideBy: 1
                },
                768 : {
                    items: 2,
                    slideBy: 2
                },
                1024 : {
                    items: 3,
                    slideBy: 3
                },
                1280 : {
                    items: 4,
                    slideBy: 4
                },
                1440 : {
                    items: 5,
                    slideBy: 1
                }
            }
        });
    }
  $('#mini-cart').text( cart.products ? cart.products.length: 0 );
  cart_total();
  $('.btn_commentForm').click(function(){
    var comment = {};
    var error = '';
    $('#frmAddStoryComment [name]').each(function(index){
      $(this).removeClass('error');
      var name = $(this).attr('name');
      var val = $(this).val();
      comment[name] = val;
      if(!val){
        $(this).addClass('error');
        error = 'Field is required';
      }
    });
    if(!error){
      console.log(comment);
      $('.btn_commentForm > span').removeClass('d-none');
      jQuery.ajax({
        url: setting.ajax_url,
        type: 'post',
        data:{data:comment,_token:setting.token,action:comment['action']},
        success: function(resulf){
          console.log(resulf);
          if(resulf){
            resulf = JSON.parse(resulf);
            if(resulf['success']){
              jQuery('.btn_commentForm > span').addClass('d-none');

              Swal.fire(
                  'Success!',
                  '',
                  'success'
              );
              location.href = location;


            }

          }

        }
      });

    }
  })





});

async function save_account(id){
  var profile = [];
  $(id+' [name]').each(function(){
    var k = $(this).attr('name');
    var t = $(this).data('type');
    var v = $(this).val();
    if(v)profile.push({name:k,value:v,type:t});
  });
  $.ajax({
    url:setting.ajax_url,
    type:'post',
    data:{data: profile ,action:'update_account',_token:setting.token},
    success: function(resulf){
      if(resulf){
        resulf = JSON.parse(resulf);
        console.log(resulf);
        if( resulf['success'] ){
          Swal.fire(
              'Success!',
              '',
              'success'
          );
        }
      }
    }
  })

}

//convert key array
async function convert_array(array) {
  var list_array = [];
  var j = 0;
  for (var i in array){
    list_array[j] = array[i];
    j++;
  }
  return list_array;
}

async function add_cart(){
  var data = {};
$('.detail-single-product [name]').each(function(){
  var k =$(this).attr('name');
  var v= $(this).val();
  data[k] = v;
});
data['key']=data['product_id']+'_'+data['size']+'_'+data['color'];
data['attributes']='Size: '+data['size']+', Color: '+data['color'];
let check = in_cart(data['key'],cart.products);
if(cart.products.length < 1 || check =='insert' )cart.products.push(data);
if(check != 'insert')cart.products[check]['quantily'] = Number( cart.products[check]['quantily'] ) + Number( data['quantily'] );
if( Number( data['quantily'] ) > 0 ){
  send_cart(cart.products);
  Swal.fire(
      'Success!',
      '',
      'success'
  );
}
}

 function in_cart(id,cart){
  for(var i= 0; i < cart.length ; i++){
    if(id == cart[i]['key'])return i;
  }
  return 'insert';
}

function send_cart(data){
  $.ajax({
    url:setting.ajax_url,
    type:'post',
    data:{data: data ,action:'add_to_cart',_token:setting.token},
    success: function(resulf){
      if(resulf){
        resulf = JSON.parse(resulf);
        console.log(resulf);
        if( resulf['success'] ){
          $('#mini-cart').text( cart.products ? cart.products.length: 0 );
        }
      }
    }
  })
}

function remove_product(button){
  var key = $(button).data('product-id');
  let check = in_cart(key,cart.products);
  let products = [];
  cart.products.forEach(item=>{
    if(item.key != key)products.push(item);
  })
  cart.products = products;
  $('#shopping-cart-items .shopping-cart-item-id-'+key).remove();
  send_cart(cart.products);
  cart_total();
}

function change_quantily(button){
  var key = $(button).data('product-id');
  let check = in_cart(key,cart.products);
  let quantily = $(button).val();
  cart.products[check]['quantily'] = Number( quantily );
  send_cart(cart.products);
  cart_total();
}

function cart_total(){
  let Subtotal = 0;
  let Tax = $('#cart_tax_rate').val();
  let Shipping = $('#cart_shipping').val();
  let Total = 0;
  if(cart.products){
    cart.products.forEach(item=>{
      Subtotal += Number( item.subtotal ) * item.quantily ;
    });
    $('#shopping-cart-subtotal').text('$'+Subtotal.toFixed(2) );
    $('#shopping-cart-tax').text('$'+ (Tax*Subtotal).toFixed(2) );
    $('#shopping-cart-shipping').text('$'+ Shipping );
    Total = Number(Shipping) + Subtotal + Tax*Subtotal;
    $('#shopping-cart-total').text('$'+ Total.toFixed(2) );
  }

}
