// check username & email to register
async function save_post(){
    jQuery('.create_new_story [name]').each( function(){
        var k = jQuery(this).attr('name');
        var v = jQuery(this).val();
        var t = jQuery(this).data('title');
        var r = jQuery(this).data('required');
        if(k != 'post_content'){
            posts[k] = {name: k, value:v,label: t,required: r};
            jQuery(this).next().addClass('d-none');
        }
        if(k == 'post_content'){
            posts[k] = {name: k, value:v,label: t,required: r};
            jQuery(this).next().next('span').addClass('d-none');
        }

    });

    if(posts){
        var error = '';
        for (var i in posts){
            if(posts[i].required & !posts[i].value){
                error += posts[i].label +' is required !';
                var div = document.querySelector('.create_new_story [name="'+i+'"]');
                jQuery(div).next().html('<span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>' + posts[i].label +' is required !').removeClass('d-none');
            }

            if(i =='post_content' & !posts[i].value){
                error += posts[i].label +' is required !';
                var div = document.querySelector('.create_new_story [name="'+i+'"]');
                jQuery(div).next().next('span').html('<span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>' + posts[i].label +' is required !').removeClass('d-none');
            }

            if(i =='audio' & posts[i].required & !posts[i].value){
                error += posts[i].name +' is required !';
                var div = document.querySelector('.create_new_story [name="story_type"]');
                jQuery(div).next().html('<span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>' + posts[i].name +' is required !').removeClass('d-none');
            }
        }

        if( !error ){
            datas =  convert_array(posts);
            jQuery('#button-save-story span').removeClass('d-none');
            jQuery.ajax({
                url: posts['action']['value'],
                type: 'post',
                data:{data:datas,_token:posts['_token']['value']},
            success: function(resulf){
                console.log(resulf);
                if(resulf){
                    jQuery('#button-save-story span').addClass('d-none');
                    resulf = JSON.parse(resulf);
                    if(resulf['redirect']){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Your work has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        setTimeout(function(){location.href= resulf['redirect'];},3000)

                    }

                    if(resulf['success']){
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Save successfully'
                        })
                    }
                }
            }
            });

        }




    }

}

//convert key array
function convert_array(array) {
    var list_array = [];
    var j = 0;
    for (var i in array){
        list_array[j] = array[i];
        j++;
    }
    return list_array;
}
// delete story
async function delete_post(id,action){
    Swal.fire({
        title: 'Are you sure?',
        text: "You won't be able to revert this!",
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
    }).then((result) => {
        if (result.value) {
            jQuery.ajax({
                url:setting.ajax_url,
                type: 'post',
                data:{action:action,_token:setting.token, id:id},
            success: function(resulf){
                if(resulf){
                    Swal.fire(
                        'Deleted!',
                        'Your work has been deleted.',
                        'success'
                    );
                    resulf = JSON.parse(resulf);
                    if(resulf['redirect']){
                        setTimeout(function(){location.href= resulf['redirect'];},2000);
                    }

                    if(resulf['reload']){
                        setTimeout(function(){location.href= location;},2000);
                    }

                }
            }
        });

        }
    })
}

jQuery.fn.change_status_comment = function(id,action) {
    var v = jQuery(this).val();
    jQuery.ajax({
        url:setting.ajax_url,
        type: 'post',
        data:{action:action,_token:setting.token, id:id,value:v},
        success: function(resulf){
            if(resulf){
                resulf = JSON.parse(resulf);
                if(resulf['success']){
                    const Toast = Swal.mixin({
                        toast: true,
                        position: 'top-end',
                        showConfirmButton: false,
                        timer: 1000,
                        timerProgressBar: true,
                        onOpen: (toast) => {
                            toast.addEventListener('mouseenter', Swal.stopTimer)
                            toast.addEventListener('mouseleave', Swal.resumeTimer)
                        }
                    })

                    Toast.fire({
                        icon: 'success',
                        title: 'Save successfully'
                    })
                }
            }
        }
    });
}


// save product
async function save_product(){
    var error = '';
    products['additional_information'] = {name:'additional_information',value:[],required:false};
    jQuery('.create_new_product [name]').each( function(){
        var k = jQuery(this).attr('name');
        var v = jQuery(this).val();
        var t = jQuery(this).data('title');
        var r = jQuery(this).data('required');

        if(k != 'description' && k != 'size' && k != 'color' && k != 'title' && k != 'content' && k!='additional-information'){
            console.log(k);
            products[k] = {name: k, value:v,label: t,required: r};
            if(products[k].required)jQuery(this).next().addClass('d-none');
        }
        if(k == 'description' || k == 'size' || k == 'color'){
            products[k] = {name: k, value:v,label: t,required: r};
            jQuery(this).next().next('span').addClass('d-none');
        }

        if(k == 'title' || k == 'content'){
            products['additional_information'].value.push({name: k, value:v,label: t,required: r});
            jQuery(this).removeClass('d-error');
            if(r & !v){
                error += t +' is required !';
                $(this).addClass('d-error');
            }
        }

    });

    if(products){
        for (var i in products){
            if(products[i].required & !products[i].value){
                error += products[i].label +' is required !';
                var div = document.querySelector('.create_new_product [name="'+i+'"]');
                jQuery(div).next().html('<span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>' + products[i].label +' is required !').removeClass('d-none');
            }

            if(i =='description' & !products[i].value){
                error += products[i].label +' is required !';
                var div = document.querySelector('.create_new_product [name="'+i+'"]');
                jQuery(div).next().next('span').html('<span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>' + products[i].label +' is required !').removeClass('d-none');
            }

            if( (i =='size' || i == 'color') & !products[i].value.length > 0){
                error += products[i].label +' is required !';
                var div = document.querySelector('.create_new_product [name="'+i+'"]');
                jQuery(div).next().next('span').html('<span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>' + products[i].label +' is required !').removeClass('d-none');
            }

        }

        if( !error ){
            datas =  convert_array(products);
            jQuery('#button-save-product span').removeClass('d-none');
            jQuery.ajax({
                url: products['action']['value'],
                type: 'post',
                data:{data:datas,_token:products['_token']['value']},
                success: function(resulf){
                    console.log(resulf);
                    if(resulf){
                        jQuery('#button-save-product span').addClass('d-none');
                        resulf = JSON.parse(resulf);
                        if(resulf['redirect']){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Your work has been saved',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            setTimeout(function(){location.href= resulf['redirect'];},3000)

                        }

                        if(resulf['success']){
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Save successfully'
                            })
                        }
                    }
                }
            });

        }




    }

}


// save order
async function save_order(){
    jQuery('.save_order [name]').each( function(){
        var k = jQuery(this).attr('name');
        var v = jQuery(this).val();
        var t = jQuery(this).data('title');
        var r = jQuery(this).data('required');
        if(k != 'description' && k != 'size' && k != 'color'){
            orders[k] = {name: k, value:v,label: t,required: r};
            if(orders[k].required)jQuery(this).next().addClass('d-none');
        }
        if(k == 'description' || k != 'size' || k != 'color'){
            orders[k] = {name: k, value:v,label: t,required: r};
            jQuery(this).next().next('span').addClass('d-none');
        }

    });

    if(orders){
        var error = '';
        for (var i in orders){
            if(orders[i].required & !orders[i].value){
                error += orders[i].label +' is required !';
                var div = document.querySelector('.save_order [name="'+i+'"]');
                jQuery(div).next().html('<span class="um-field-arrow"><i class="um-faicon-caret-up"></i></span>' + orders[i].label +' is required !').removeClass('d-none');
            }

        }

        if( !error ){
            datas =  convert_array(orders);
            jQuery('#button-save-order span').removeClass('d-none');
            jQuery.ajax({
                url: orders['action']['value'],
                type: 'post',
                data:{data:datas,_token:orders['_token']['value']},
                success: function(resulf){
                    console.log(resulf);
                    if(resulf){
                        jQuery('#button-save-order span').addClass('d-none');
                        resulf = JSON.parse(resulf);
                        if(resulf['redirect']){
                            Swal.fire({
                                position: 'top-end',
                                icon: 'success',
                                title: 'Your work has been saved',
                                showConfirmButton: false,
                                timer: 1500
                            })
                            setTimeout(function(){location.href= resulf['redirect'];},3000)

                        }

                        if(resulf['success']){
                            const Toast = Swal.mixin({
                                toast: true,
                                position: 'top-end',
                                showConfirmButton: false,
                                timer: 3000,
                                timerProgressBar: true,
                                onOpen: (toast) => {
                                    toast.addEventListener('mouseenter', Swal.stopTimer)
                                    toast.addEventListener('mouseleave', Swal.resumeTimer)
                                }
                            })

                            Toast.fire({
                                icon: 'success',
                                title: 'Save successfully'
                            })
                        }
                    }
                }
            });

        }




    }

}

// add item in FAQ in each array
async function add_item_fqa(dom){
var div =  document.createElement('div');
div.className = 'form-group mb-4';
var button =  document.createElement('button');
button.className = 'btn btn-tool';
button.type = 'button';
button.innerHTML = '<i class="far fa-times-circle"></i> Delete';
button.setAttribute('onclick','remove_item(this)');
div.appendChild(button);

var input = document.createElement('input');
input.type = 'text';
input.name = 'title';
input.className = 'form-control mt-1 mb-1';
input.setAttribute('data-faq',dom);
input.setAttribute('data-title','Title');
input.setAttribute('data-required','true');
input.setAttribute('placeholder','Title');
    div.appendChild(input);
var span = document.createElement('span');
span.className = 'um-field-error d-none';
div.appendChild(span);



var textarea = document.createElement('textarea');
    textarea.name = 'content';
    textarea.className = 'form-control mt-3 mb-1';
    textarea.setAttribute('data-faq',dom);
    textarea.setAttribute('data-title','Content');
    textarea.setAttribute('data-required','true');
    textarea.setAttribute('placeholder','Content');
    div.appendChild(textarea);
var span = document.createElement('span');
    span.className = 'um-field-error d-none';
    div.appendChild(span);

document.querySelector('.item-faq-'+dom+' .card-body').appendChild(div);
}
// save setting FAQ
async  function save_faq(){
    var error = '';
    clear_faq();
    jQuery('#setting-faq [name]').each( function(index){
        var f = jQuery(this).data('faq');
        var k = jQuery(this).attr('name');
        var v = jQuery(this).val();
        var t = jQuery(this).data('title');
        var r = jQuery(this).data('required');
        faq[f].value.push({name: k, value:v,label: t,required: r});
        $(this).removeClass('d-error');
        if(r & !v){
            error += t +' is required !';
            $(this).addClass('d-error');
        }
    });
    if(!error){
        jQuery('#button-save-faq span').removeClass('d-none');
        jQuery.ajax({
            url: setting.save_faq_ajax_url,
            type: 'post',
            data:{data:faq,_token:setting.token},
            success: function(resulf){
                console.log(resulf);
                if(resulf){
                    resulf = JSON.parse(resulf);
                    if(resulf['success']){
                        jQuery('#button-save-product span').addClass('d-none');
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Save successfully'
                        })
                    }
                }

                }
        });
    }

}

// save form data  ctf
async function save_ctf(){
    var error = '';
    var ctf = [];
    jQuery('#setting-ctf [name]').each( function(index){
        var k = jQuery(this).attr('name');
        var v = jQuery(this).val();
        var t = jQuery(this).data('title');
        var r = jQuery(this).data('required');
        ctf[k] = {name: k, value:v,label: t,required: r};
        if(k != 'message'){
            $(this).removeClass('d-error');
            $(this).next().addClass('d-none');
        }
        if(k == 'message'){
            $(this).next().next().addClass('d-none');
        }

        if(r & !v){
            error += t +' is required !';
            $(this).addClass('d-error');
            if(k == 'message'){
                $(this).next().next().removeClass('d-none').text(t +' is required !');
            }else{
                $(this).next().removeClass('d-none').text(t +' is required !');
            }
        }

        if(k =='email' & r & !ValidateEmail(v)){
            error += t +' is unvalid !';
            $(this).next().removeClass('d-none').text(t +' is unvalid !');
        }
    });
    console.log(error);

    if(!error){
        datas =  convert_array(ctf);
        console.log(datas);
        jQuery('#button-save-ctf span').removeClass('d-none');
        jQuery.ajax({
            url: setting.ajax_url,
            type: 'post',
            data:{data:datas,_token:setting.token, action:ctf['action'].value},
            success: function(resulf){
                console.log(resulf);
                if(resulf){
                    resulf = JSON.parse(resulf);
                    if(resulf['success']){
                        jQuery('#button-save-ctf span').addClass('d-none');
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Save successfully'
                        })
                    }
                }

            }
        });
    }
}

// check valicate email
function ValidateEmail(inputText)
{
    var mailformat = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
    if(inputText.match(mailformat))
    {
        return true;
    }
    else
    {
        return false;
    }
}

// check valicate URL
function isValidUrl(string) {
    try {
        new URL(string);
    } catch (_) {
        return false;
    }

    return true;
}

// add customer data form ctf
async  function save_data_row_ctf(){
    var error = '';
    var data = [];
    jQuery('#display-data-ctf [name]').each( function(index){
        var k = jQuery(this).attr('name');
        var v = jQuery(this).val();
        var t = jQuery(this).data('title');
        var r = jQuery(this).data('required');
        data[k]= {name: k, value:v,label: t,required: r};
        $(this).removeClass('d-error');
        if(r & !v){
            error += t +' is required !';
            $(this).addClass('d-error');
        }

        if(k =='email' && v && !ValidateEmail(v)){
            error += t +' is unvalid !';
            $(this).addClass('d-error');
        }


    });
    if(!error){
        console.log(data);
        datas =  convert_array(data);
        jQuery('#button-data-form-customer span').removeClass('d-none');
        jQuery.ajax({
            url: setting.ajax_url,
            type: 'post',
            data:{data:datas,_token:setting.token, action:data['action'].value},
            success: function(resulf){
                console.log(resulf);
                if(resulf){
                    resulf = JSON.parse(resulf);
                    if(resulf['success']){
                        jQuery('#button-data-form-customer span').addClass('d-none');
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Save successfully'
                        })


                    }

                    if(resulf['redirect']){
                        Swal.fire({
                            position: 'top-end',
                            icon: 'success',
                            title: 'Your work has been saved',
                            showConfirmButton: false,
                            timer: 1500
                        })
                        setTimeout(function(){location.href= resulf['redirect'];},3000)
                    }
                }

            }
        });
    }

}


async function save_system_settings(){
    var error = '';
    var data = [];
    jQuery('.display_system_settings [name]').each( function(index){
        var k = jQuery(this).attr('name');
        var v = jQuery(this).val();
        var t = jQuery(this).data('title');
        var r = jQuery(this).data('required');
        data[k]= {name: k, value:v,label: t,required: r};
        $(this).removeClass('d-error');
        if(r & !v){
            error += t +' is required !';
            $(this).addClass('d-error');
        }

        if(k =='site_email' && v && !ValidateEmail(v)){
            error += t +' is unvalid !';
            $(this).addClass('d-error');
        }

        if(k =='site_url' && v && !isValidUrl(v)){
            error += t +' is unvalid !';
            $(this).addClass('d-error');
        }

    });

    if(!error){
        console.log(data);
        datas =  convert_array(data);
        jQuery('#button-display_system_settings span').removeClass('d-none');
        jQuery.ajax({
            url: setting.ajax_url,
            type: 'post',
            data:{data:datas,_token:setting.token, action:data['action'].value},
            success: function(resulf){
                console.log(resulf);
                if(resulf){
                    resulf = JSON.parse(resulf);
                    if(resulf['success']){
                        jQuery('#button-display_system_settings span').addClass('d-none');
                        const Toast = Swal.mixin({
                            toast: true,
                            position: 'top-end',
                            showConfirmButton: false,
                            timer: 3000,
                            timerProgressBar: true,
                            onOpen: (toast) => {
                                toast.addEventListener('mouseenter', Swal.stopTimer)
                                toast.addEventListener('mouseleave', Swal.resumeTimer)
                            }
                        })

                        Toast.fire({
                            icon: 'success',
                            title: 'Save successfully'
                        })


                    }

                }

            }
        });
    }
}