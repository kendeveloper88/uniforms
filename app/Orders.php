<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Orders extends Model
{
    // get all orders
    public static function get_orders($where=''){
        $orders = DB::table('product_orders')->orderByDesc('updated_at')->paginate(12);
        return $orders;
    }
    // get order by id
    public static function get_order($order_id){
        $order = DB::table('product_orders')->where('id',$order_id)->first();
        return $order;
    }

    // status order
    public static function order_status(){
        $order_status = [
            0 => 'Pending payment',
            1 => 'Failed',
            2 => 'Processing',
            3 => 'Completed',
            4 => 'On hold',
            5 => 'Canceled',
            6 => 'Refunded',
            7 => 'Gift',
        ];
        return $order_status;
    }

    // status product
    public static function payment_type(){
        $product_type = [
            0 => 'Cash on delivery',
            1 => 'Direct bank transfer',
            2 => 'PayPal',
        ];
        return $product_type;
    }

    // get meta orders
    public static function get_meta_product_order($order_id,$meta_key,$re=''){
        $order_meta = DB::table('product_order_meta')->where('order_id',$order_id)->where('meta_key',$meta_key)->first();
        if($order_meta)return $order_meta->meta_value ;
        return $re;
    }
    // update meta orders
    public static function update_meta_product_order($order_id,$meta_key,$meta_value){
        $order_meta = self::get_meta_product_order($order_id,$meta_key);
        if($order_meta){
            DB::table('product_order_meta')->where('order_id',$order_id)->where('meta_key',$meta_key)->update(['meta_value'=>$meta_value]);
        }else{
            $order_meta =  self::add_meta_product_order($order_id,$meta_key,$meta_value);
        }
        return $order_meta;
    }

    // get meta orders
    public static function add_meta_product_order($order_id,$meta_key,$meta_value){
        $order_meta = self::get_meta_product_order($order_id,$meta_key);
        if(!$order_meta){
            $data = array(
                'order_id' => $order_id,
                'meta_key' => $meta_key,
                'meta_value' => $meta_value,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $meta_id = DB::table('product_order_meta')->insertGetId($data);
            return $meta_id;
        }
        return false;
    }

}
