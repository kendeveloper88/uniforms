<?php
use Illuminate\Support\Facades\Storage;
/******************************************
 * Generate SEO URL...
 ******************************************/
if (!function_exists('generateSEOURL'))
{
    function generateSEOURL($strURL='')
    {
        $arrFind = array(' ', ',', '.', '"', "'", '?', '!');
        $arrReplace = array('-', '', '', '', '', '', '');
        $strURL = strtolower(trim($strURL));
        $strURL = str_replace($arrFind, $arrReplace, $strURL);
        return $strURL;
    }
}

// Illuminate/Support/helpers.php

if (! function_exists('str_slug')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @return string
     */
    function str_slug($title, $separator = '-')
    {
        return Str::slug($title, $separator);
    }
}

//Check exist intable unique and create random number
if (! function_exists('check_field_table')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @return string
     */
    function check_field_table($title, $colume, $table)
    {
        $slug_r = $slug = str_slug($title,  '-');
        for($i=1;$i<20;$i++){
            $select =  DB::table($table)->where($colume ,$slug_r)->get()->count();
            if($select){
                $slug_r = $slug.'-'.$i;
            }else{
                return $slug_r;
            }
        }


    }
}

//Get data row by slug table
if (! function_exists('get_data_by_slug')) {
    /**
     * Generate a URL friendly "slug" from a given string.
     *
     * @param  string  $title
     * @param  string  $separator
     * @return string
     */
    function get_data_by_slug($slug, $colume, $table)
    {
        $select =  DB::table($table)->where($colume ,$slug)->get();
        return $select;


    }
}

//Get type file media
if (! function_exists('get_name_media')) {
    function get_name_media($path)
    {
        $array = explode('/',$path);
        return end($array);
    }
}

//Get type file media
if (! function_exists('get_type_media')) {
    function get_type_media($type)
    {
        $array = explode('/',$type);
        return $array[0];
    }
}

//Get type file media
if (! function_exists('show_img_media')) {
    function show_img_media($type,$url)
    {
        $type = get_type_media($type);
        if($type == 'image'){
            return $url;
        }
        return url( 'uploads/use/'.$type.'.png');
    }
}

//Select type media
if (! function_exists('select_type_media')) {
    function select_type_media()
    {
        $type_array = array(
            'All',
            'image'=>'Images',
            'video'=>'Video',
            'audio'=>'Audio',
            'application'=>'Document',
        );
        return $type_array;
    }
}

//get size by path
if (! function_exists('get_size_media')) {
    function get_size_media($file_path)
    {
        if(file_exists($file_path)){
            $file_size =  File::size(public_path($file_path));
            return number_format($file_size / 1048576,2).' MB';
        }
        return false;

    }
}


//delete file media by path
if (! function_exists('delete_file_media')) {
    function delete_file_media($file_path)
    {
        if(file_exists($file_path)){
           File::delete(public_path($file_path));
            return true;
        }
        return false;

    }
}


//Get meta story by meta_key
if (! function_exists('get_meta_post')) {
    function get_meta_post($post_id,$meta_key)
    {
        $meta = App\Posts::get_meta_post($post_id,$meta_key);
        if($meta)return $meta->meta_value;
        return false;

    }
}

//Update meta story by meta_key
if (! function_exists('update_meta_post')) {
    function update_meta_post($post_id,$meta_key,$meta_value)
    {
        $meta = App\Posts::update_meta_post($post_id,$meta_key,$meta_value);
        return $meta;

    }
}

//ADD meta story by meta_key
if (! function_exists('add_meta_post')) {
    function add_meta_post($post_id,$meta_key,$meta_value)
    {
        $meta = App\Posts::add_meta_post($post_id,$meta_key,$meta_value);
        return $meta;

    }
}

// echo content function
if (! function_exists('_e')) {
    function _e($content){echo $content;}
}


//show media ajax then get
if (! function_exists('display_media_modal')) {
    function display_media_modal($medias)
    {
        ob_start();
        ?>
        <!--Show media-->
        <div id="grid-medias" class="filter-container p-0 row">
            <?php if (count($medias) > 0): ?>
            <?php foreach ($medias as $media): $media->ftype = get_type_media($media->type); ?>
            <div class="item-media filtr-item mb-5 col-sm-2">
                <div class="link"  style="background-image: url('<?php _e(show_img_media($media->type,$media->link)) ?>');"  onClick='<?php _e( "$(this).select_media(`".json_encode($media)."`)" ) ?>' data-ID="<?php _e($media->id) ?>">
                    <?php if( get_type_media($media->type) != 'image'): ?>
                    <span class="name-media d-block"><?php _e(get_name_media( $media->path )) ?></span>
                    <?php endif; ?>
                </div>
            </div>
            <?php endforeach; ?>
            <?php endif; ?>
        </div>
<?php
        $resulf = ob_get_contents();
        ob_end_clean();
        return $resulf;

    }
}

// display category product
if (! function_exists('display_category_product')) {
    function display_category_product($id,$slug='name'){
        $category = App\Relationships::get_relationships($id,'product');
        $term_id = $category ? $category->term_id:'';
        if($term_id){
            $data = App\Product::get_product_categories_detail($term_id);
            return $data->$slug;
        }
        return '';

    }
}


// display attribute product
if (! function_exists('display_attribute_product')) {
    function display_attribute_product($id,$name,$re=''){
        $meta_value = App\Product::get_meta_product($id,$name);
        if($meta_value)return json_decode($meta_value);
        return $re;

    }
}

// checl attribute select
// display attribute product
if (! function_exists('check_search_array')) {
    function check_search_array($val,$array,$re=''){
        $key = array_search($val,$array);
        return $array[$key];
    }
}

// format currency
if (! function_exists('format_currency')) {
    function format_currency($input, $decimal=0, $cur=''){
        if(!$input)return 0;
        $currency = number_format($input, $decimal, '.', ',');
        return $cur.$currency;
    }
}

// display product in orders
if (! function_exists('display_product_in_order')) {
    function display_product_in_order($data){
        $resulf = [];
       ob_start();
        if(isset($data)){
            $total = 0;
            foreach($data as $value): $total += $value->subtotal;  ?>
                <tr>
                    <td><?php _e($value->quantily) ?></td>
                    <td><?php _e($value->title) ?></td>
                    <td><?php _e($value->attributes) ?></td>
                    <td><?php _e( format_currency( $value->subtotal,2,'$') ) ?></td>
                </tr>
          <?php  endforeach;
            $resulf['subtotal'] =  $total;
        }
       $resulf['html'] = ob_get_contents();
       ob_end_clean();
       return $resulf;
    }
}



/// get curren datetime
if( !function_exists('get_current_datetime')){
    function get_current_datetime($date){
        $time = round(time()/60,0);
        $minutes = round(strtotime($date) / 60,0);
        $left = $time - $minutes;
        if($left == 0)$left=1;
        switch ($left){
            case $left < 60:
                return ( round($left).' minutes ago');
            break;

            case $left < 24*60:
                return (round($left/60).' hours ago');
            break;

            case $left < 24*60*7:
                return ( round($left/(24*60)).' days ago');
            break;

            case $left < 24*60*30:
                return (round($left/(24*60*7) ).' weeks ago');
            break;

            case $left < 24*60*180:
                return (round($left/(24*60*30) ).' months ago');
            break;

            default:
                return $date;
            break;
        }
    }
}

/// get optiond
if( !function_exists('get_option')){
    function get_option($user_id,$key){
     return App\Options::get_option($user_id,$key);
    }
}

/// get field optiond
if( !function_exists('get_field_option')){
    function get_field_option($key){
        return App\Options::get_field_option($key);
    }
}


/// display menu
if( !function_exists('display_menu')){
    function display_menu($slug=''){
       ob_start();
       $menus = App\Options::get_menu();
        ?>
        <ul class="menu-main">
            <?php foreach ($menus as $key => $value): ?>
            <li class="nav-item <?php $key == $slug ?_e('active'):'' ?>"><a class="nav-link" href="<?php _e( url($key)) ?>"><?php _e($value) ?></a></li>
            <?php endforeach; ?>
        </ul>
        <?php
       $resulf = ob_get_contents();
       ob_end_clean();
       return $resulf;
    }
}


// limit text
    if( !function_exists('limit_text') ){
        function limit_text($text, $limit) {
            if (str_word_count($text, 0) > $limit) {
                $words = str_word_count($text, 2);
                $pos = array_keys($words);
                $text = substr($text, 0, $pos[$limit]) . '...';
            }
            return $text;
        }
    }

// get user meta
if( !function_exists('get_user_meta') ){
    function get_user_meta($user_id,$meta_key){
        $user_meta =  App\User::get_user_meta($user_id,$meta_key);
        return $user_meta;
    }

}
// upadte user meta
if( !function_exists('update_user_meta') ){
    function update_user_meta($user_id,$meta_key,$meta_value){
        $user_meta =  App\User::update_user_meta($user_id,$meta_key,$meta_value);
        return $user_meta;
    }

}

// show item product
if( !function_exists('showItemProduct') ){
    function showItemProduct($product){
        ob_start();
        if(!$product['price'])$product['price']= 0;
        ?>
        <span class="SKU">SKU: <?= $product['sku']  ?></span>
        <div class="thumbnail-product text-center"><img src="<?= $product['image']  ?>"></div>
        <div class="category"><?= $product['category']  ?></div>
        <div class="title-product"><?= $product['title']  ?></div>
        <div class="price-product">$<?= number_format($product['price'], 2, ',', ' '); ?></div>
    <?php
        $resulf = ob_get_contents();
        ob_end_clean();
        return $resulf;
    }

}

