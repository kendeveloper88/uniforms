<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Media extends Model
{
    public $table = 'media';
    public static function get_media($where=''){
        if($where){
            $medias = DB::table('media')->where('type','LIKE', $where.'%')->orderBy('id', 'desc')->paginate(12);
        }else{
            $medias = DB::table('media')->orderBy('id', 'desc')->paginate(12);
        }
        return $medias;
    }

    public static function get_media_detail($id){
        $media = DB::table('media')->where('id', $id)->first();
        return $media;
    }
}
