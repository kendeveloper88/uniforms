<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Relationships extends Model
{
    // add relationships
    public static function save_relationships($object_id,$term_id,$type=""){
        $relationships = self::get_relationships($object_id,$type);
        $resulf = [];
        $data = array(
            'object_id' => $object_id,
            'term_id' => $term_id,
            'type' => $type,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
        );
        if(!$relationships){
            DB::table('relationships')->insert($data);
            return \GuzzleHttp\json_encode($data);
        }
        DB::table('relationships')->where('object_id',$object_id)->where('term_id',$term_id)->update($data);
        $resulf['update'] = 'successfully' ;
        return \GuzzleHttp\json_encode($resulf);

    }
    // get relationships
    public static function get_relationships($object_id,$type=""){
        $where = DB::table('relationships')->where('object_id',$object_id);
        if(isset($type))$where->where('type',$type);
        $relationships = $where->first();
        if($relationships) return $relationships ;
        return false;
    }


}
