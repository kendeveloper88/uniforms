<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Product extends Model
{
    // get all product
    public static function get_products($where=''){
        $products = DB::table('products')->orderByDesc('updated_at')->paginate(12);
        return $products;
    }

    // get all categories
    public static function get_product_categories($where=''){
        $products = DB::table('product_categories')->orderByDesc('id')->paginate(12);
        return $products;
    }

    // get all categories
    public static function get_product_categories_detail($id){
        $product_categories = DB::table('product_categories')->where('id',$id)->first();
        if($product_categories)return $product_categories;
        return $product_categories;
    }

    // get product by id
    public static function get_product($product_id){
        $product = DB::table('products')->where('id',$product_id)->first();
        return $product;
    }

    // get product by slug
    public static function get_product_bySlug($slug=''){
        $product = DB::table('products')->where('slug',$slug)->first();
        return $product;
    }

    // status product
    public static function product_status(){
        $product_status = [
            0 => 'Draft',
            1 => 'Published',
            2 => 'Hidden',
            3 => 'Deleted',
        ];
        return $product_status;
    }

    // status product
    public static function product_type(){
        $product_type = [
            0 => 'Simple',
            1 => 'Variations',
        ];
        return $product_type;
    }

    //  product attributes
    public static function product_attributes(){
        $product_attributes = [
            'size' => [
                'title'=> 'Size',
                'value'=> [
                    's' => 'S',
                    'm' => 'M',
                    'l' => 'L',
                    'xl' => 'XL',
                    'xxl' => 'XXL',
                ],
                'default'=> 'm',
            ],
            'color' => [
                'title' => 'Color',
                'value' => [
                    'black' => 'Black',
                    'red' => 'Red',
                    'white' => 'White',
                ],
                'default'=> 'white',
            ],
        ];
        return $product_attributes;
    }

    // get meta product
    public static function get_meta_product($product_id,$meta_key,$re=''){
        $product_meta = DB::table('product_meta')->where('product_id',$product_id)->where('meta_key',$meta_key)->first();
        if($product_meta)return $product_meta->meta_value ;
        return $re;
    }
    // update meta product
    public static function update_meta_product($product_id,$meta_key,$meta_value){
        $product_meta = self::get_meta_product($product_id,$meta_key);
        if($product_meta){
            DB::table('product_meta')->where('product_id',$product_id)->where('meta_key',$meta_key)->update(['meta_value'=>$meta_value]);
        }else{
            $product_meta =  self::add_meta_product($product_id,$meta_key,$meta_value);
        }
        return $product_meta;
    }

    // get meta product
    public static function add_meta_product($product_id,$meta_key,$meta_value){
        $product_meta = self::get_meta_product($product_id,$meta_key);
        if(!$product_meta){
            $data = array(
                'product_id' => $product_id,
                'meta_key' => $meta_key,
                'meta_value' => $meta_value,
                'created_at' => date('Y-m-d H:i:s'),
                'updated_at' => date('Y-m-d H:i:s'),
            );
            $meta_id = DB::table('product_meta')->insertGetId($data);
            return $meta_id;
        }
        return false;
    }



}
