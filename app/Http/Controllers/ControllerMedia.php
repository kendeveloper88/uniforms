<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use App\User;

class ControllerMedia extends Controller
{
    public function doUpload(Request $request)
    {
        //Kiểm tra file
        if ($request->hasFile('UploadMedia')) {
            $file = $request->UploadMedia;
            $path = 'uploads/'.date('Y').'/'.date('m');
            //Name files
            $filename = explode('.',$file->getClientOriginalName())[0];
            $filename = check_field_table($filename,'title','media');
            // End file
            $end_file = '.'.$file->getClientOriginalExtension();
            //Type file
            $type =  $file->getMimeType();
            // insert file
           $errors =  $file->move($path, $filename.$end_file);

            // save database
            $user_id = Auth::id();
            $media = [
            'user_id' => $user_id,
            'path' => $path.'/'.$filename.$end_file,
            'type' => $type,
            'title' => $filename,
            'link' => url($path.'/'.$filename.$end_file),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s'),
            ];

            $media_id = DB::table('media')->insertGetId($media);
            $media['id'] = $media_id;
            $media['end_file'] = $file->getClientOriginalExtension();
            $media['name_media'] = $filename.$end_file;
            $media['size'] = get_size_media($media['path']);
            $media['ftype'] = get_type_media($media['type']);
            $media['description'] = '';
            $media['author'] = Auth::user()->name;
            $media['success'] = 'successfully';
            echo \GuzzleHttp\json_encode($media);

        }
        die();
    }
}
