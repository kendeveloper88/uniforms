<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Relationships;
use App\Product;

class ControllerProduct extends Controller
{
    public function add_product_category(Request $request){
        if ($request['data']) {
            foreach ($request['data'] as $value) {
                $data[$value['name']] = $value['value'];
            }
            $data['slug'] = check_field_table($data['name'],'slug','product_categories');
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            unset($data['_token']);
            $data['id'] =  DB::table('product_categories')->insertGetId($data);
            $data['success'] = 'successfully';
            echo \GuzzleHttp\json_encode($data);
        }

    }


    public function add_product(Request $request){
        $resulf=[];
        if ($request['data']) {
            foreach ($request['data'] as $value) {
                $data[$value['name']] = $value['value'];
            }
            $data['slug'] = check_field_table($data['name'],'slug','products');
            $data['status'] = 0;
            $data['created_at'] = date('Y-m-d H:i:s');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['featured_image'] =  (isset($data['button_featured_image']))?$data['button_featured_image']:null;
            if(isset($data['product_category'])){
                $category = $data['product_category'];
                unset($data['product_category']);
            }
            $price = (isset($data['price']))?$data['price']:null;
            $shipping = (isset($data['shipping']))?$data['shipping']:null;
            $color = (isset($data['color']))? \GuzzleHttp\json_encode($data['color']) :null;
            $size = (isset($data['size']))? \GuzzleHttp\json_encode($data['size']) :null;
            $gallery = (isset($data['button_gallery']))? \GuzzleHttp\json_encode($data['button_gallery']) :null;
            $additional_information = (isset($data['additional_information']))? \GuzzleHttp\json_encode($data['additional_information']) :null;
            unset($data['action']);
            unset($data['files']);
            unset($data['_token']);
            unset($data['button_featured_image']);

            unset($data['price']);
            unset($data['shipping']);
            unset($data['color']);
            unset($data['size']);
            unset($data['button_gallery']);
            unset($data['additional_information']);

            $product_id =  DB::table('products')->insertGetId($data);
            // save category
            if(isset($category)){
                Relationships::save_relationships($product_id,$category,'product');
            }

            if($price)Product::update_meta_product($product_id,'price',$price);
            if($shipping)Product::update_meta_product($product_id,'shipping',$shipping);
            if($color)Product::update_meta_product($product_id,'color',$color);
            if($size)Product::update_meta_product($product_id,'size',$size);
            if($gallery)Product::update_meta_product($product_id,'gallery',$gallery);
            if($additional_information)Product::update_meta_product($product_id,'additional_information',$additional_information);

            $resulf['redirect'] = url('admin/product/'.$product_id.'/edit');
        }

        echo \GuzzleHttp\json_encode($resulf);
    }

    public function save_product(Request $request){
        $resulf=[];
        if ($request['data']) {
            foreach ($request['data'] as $value) {
                $data[$value['name']] = $value['value'];
            }
            $data['slug'] = check_field_table($data['name'],'slug','products');
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['featured_image'] =  (isset($data['button_featured_image']))?$data['button_featured_image']:'';
            $gallery = (isset($data['button_gallery']))? \GuzzleHttp\json_encode($data['button_gallery']) :null;
            $price = (isset($data['price']))?$data['price']:null;
            $shipping = (isset($data['shipping']))?$data['shipping']:null;
            $color = (isset($data['color']))? \GuzzleHttp\json_encode($data['color']) :null;
            $size = (isset($data['size']))? \GuzzleHttp\json_encode($data['size']) :null;
            $additional_information = (isset($data['additional_information']))? \GuzzleHttp\json_encode($data['additional_information']) :null;

            unset($data['action']);
            unset($data['files']);
            unset($data['_token']);
            unset($data['button_featured_image']);
            unset($data['price']);
            unset($data['shipping']);
            unset($data['color']);
            unset($data['size']);
            unset($data['button_gallery']);
            unset($data['additional_information']);
            // save category
            if(isset($data['product_category'])){
                Relationships::save_relationships($data['id'],$data['product_category'],'product');
                unset($data['product_category']);
            }
            // save attribute
            if($price)Product::update_meta_product($data['id'],'price',$price);
            if($shipping)Product::update_meta_product($data['id'],'shipping',$shipping);
            if($color)Product::update_meta_product($data['id'],'color',$color);
            if($size)Product::update_meta_product($data['id'],'size',$size);
            if($gallery)Product::update_meta_product($data['id'],'gallery',$gallery);
            if($additional_information)Product::update_meta_product($data['id'],'additional_information',$additional_information);

            if(!$data['featured_image'])unset($data['featured_image']);
            DB::table('products')->where('id', $data['id'])->update($data);
            $resulf['success'] = 'Successfully';
        }

        echo \GuzzleHttp\json_encode($resulf);
    }


}
