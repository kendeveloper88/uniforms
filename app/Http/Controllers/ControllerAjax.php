<?php

namespace App\Http\Controllers;
use App\Posts;
use Illuminate\Http\Request;
use App\Stories;
use App\Media;
use App\Options;
use App\DataForm;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

class ControllerAjax extends Controller
{
    public function admin_ajax(Request $request){
        // delete story by id
        if(isset($request['action']) && $request['action']=='detele_story'){
            $resulf=[];
            DB::table('posts')->where('id',$request['id'])->delete();
            $resulf['redirect']= route('admin.posts');
            echo \GuzzleHttp\json_encode($resulf);
        }

        // delete story by id
        if(isset($request['action']) && $request['action']=='delete_comment'){
            $resulf=[];
            DB::table('posts_comment')->where('id',$request['id'])->delete();
            $resulf['reload']= true;
            echo \GuzzleHttp\json_encode($resulf);
        }


        // update status comment stories
        if(isset($request['action']) && $request['action']=='status_comment'){
            $resulf=[];
            DB::table('posts_comment')->where('id',$request['id'])->update(['status'=>$request['value']]);
            $resulf['success']= true;
            echo \GuzzleHttp\json_encode($resulf);
        }


        // update media
        if(isset($request['action']) && $request['action']=='update_media'){
            $resulf=[];
            DB::table('media')->where('id',$request['id'])->update([ 'title'=>$request['title'],'description'=>$request['description'] ]);
            $resulf['success']= true;
            echo \GuzzleHttp\json_encode($resulf);
        }

        // delete media
        if(isset($request['action']) && $request['action']=='delete_media'){
            $resulf=[];
            DB::table('media')->where('id',$request['id'])->delete();
            $resulf['success']= delete_file_media($request['path']);
            echo \GuzzleHttp\json_encode($resulf);
        }


        // get media
        if(isset($request['action']) && $request['action']=='get_medias'){
            $resulf =  Media::get_media($request['type']);
            $html = display_media_modal($resulf);
            echo $html;
        }


        // delete story by id
        if(isset($request['action']) && $request['action']=='delete_category'){
            $resulf=[];
            DB::table('product_categories')->where('id',$request['id'])->delete();
            $resulf['success']= true;
            echo \GuzzleHttp\json_encode($resulf);
        }

        // update category
        if(isset($request['action']) && $request['action']=='update_product_category'){
            foreach ($request['data'] as $value) {
                $data[$value['name']] = $value['value'];
            }
            $data['slug'] = check_field_table($data['name'],'slug','product_categories');
            $data['updated_at'] = date('Y-m-d H:i:s');
            DB::table('product_categories')->where('id',$data['id'])->update($data);
            $data['success']= true;
            echo \GuzzleHttp\json_encode($data);
        }

        // delete story by id
        if(isset($request['action']) && $request['action']=='detele_product'){
            $resulf=[];
            DB::table('products')->where('id',$request['id'])->delete();
            $resulf['redirect']= route('admin.products');
            echo \GuzzleHttp\json_encode($resulf);
        }

        // delete story by id
        if(isset($request['action']) && $request['action']=='detele_order'){
            $resulf=[];
            DB::table('product_orders')->where('id',$request['id'])->delete();
            $resulf['redirect']= route('admin.orders');
            echo \GuzzleHttp\json_encode($resulf);
        }


        // update contact form
        if(isset($request['action']) && $request['action']=='update_data_ctf'){
            $resulf=[];
            foreach ($request['data'] as $value) {
                $data[$value['name']] = $value['value'];
            }
            unset($data['action']);
            unset($data['files']);
            $user_id = Auth::id();
            $meta_key = 'option_ctf_'.$data['option_ctf'];
            $meta_value = \GuzzleHttp\json_encode($data);
            Options::update_option($user_id,$meta_key,$meta_value);
            $resulf['success']= route('admin.contact-form');
            $resulf['data']= $data;
            echo \GuzzleHttp\json_encode($resulf);
        }

        // add data form
        if(isset($request['action']) && $request['action']=='add_data_form_ctf'){
            DataForm::add_data_form($request);
        }

        // update data form
        if(isset($request['action']) && $request['action']=='update_data_form_ctf'){
            DataForm::update_data_form($request);
        }

        // delete data form customer by id
        if(isset($request['action']) && $request['action']=='detele_data_form'){
            $resulf=[];
            DB::table('data_form')->where('id',$request['id'])->delete();
            $resulf['redirect']= route('admin.data-form');
            echo \GuzzleHttp\json_encode($resulf);
        }


        // update contact form
        if(isset($request['action']) && $request['action']=='update_system_settings'){
            $resulf=[];
            foreach ($request['data'] as $value) {
                $data[$value['name']] = $value['value'];
            }
            unset($data['action']);
            $user_id = Auth::id();
            foreach ($data as $meta_key => $meta_value){
                if($meta_value)Options::update_option($user_id,$meta_key,$meta_value);
            }

            $resulf['success']= route('admin.system-settings');
            $resulf['data']= $data;
            echo \GuzzleHttp\json_encode($resulf);
        }


        // add commment story
        if(isset($request['action']) && $request['action']=='add_comment_post'){
            $resulf = [];
            $data = $request['data'];
            $id = Posts::add_post_comment($data);
            if($id)$resulf['success'] = 'successfully';
           echo  \GuzzleHttp\json_encode($resulf);

        }

        // get all user meta
        if(isset($request['action']) && $request['action']=='get_all_user_meta'){
            $resulf = [];
            $data = $request['data'];
            $user_id = Auth::id();
            foreach ($data as $key=>$value){
                $data[$key]['value'] = get_user_meta($user_id,$value['name']);
            }
            echo  \GuzzleHttp\json_encode($data);

        }

        // update account
        if(isset($request['action']) && $request['action']=='update_account'){
            $resulf = [];
            $data = $request['data'];
            $user_id = Auth::id();
            $data_user = [];
            foreach ($data as $key=>$value){
                if($value['type'] == 'meta')update_user_meta($user_id,$value['name'],$value['value']);
                if($value['type'] == 'main')$data_user[$value['name']] = $value['value'];
            }
            if(isset($data_user['password']))$data_user['password'] = bcrypt($data_user['password']);
            if($data_user)User::update_user($user_id,$data_user);
            $resulf['success'] = 'successfully';
            echo  \GuzzleHttp\json_encode($resulf);

        }


        // add to cart
        if(isset($request['action']) && $request['action']=='add_to_cart'){
            $resulf = [];
            $data['products'] = $request['data'];
            $user = Auth::check();
            if($user)$data['user'] = Auth::user();
            session()->put('cart', $data);

            $resulf['success'] = 'successfully';
            echo  \GuzzleHttp\json_encode($resulf);

        }





    }
}
